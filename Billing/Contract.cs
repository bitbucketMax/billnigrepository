﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
   public class Contract
    {
        public double bill;
        public int idcontract;
        public string passwd, phonenumber,dateconclude,podklsms,podklcall,podklinternet;
     

        public Tariff tariff;
        public Client client;
        public Provider provider;
        public List <Event> events = new List<Event>();
        private string nesovpad="Старый пароль неверен";
        private string ismeneno="Изменено";        



        public Contract(double bill,int idcontract,string passwd,string phonenumber,string datec,string podcall,string podsms,string podinternet)
        {
            this.bill = bill;
            this.idcontract = idcontract;
            this.passwd = passwd;
            this.phonenumber = phonenumber;
            this.dateconclude = datec;
            this.podklcall = podcall;
            this.podklsms = podsms;
            this.podklinternet = podinternet;

        }
        public string[] getArr()
        {
            string[] a = { phonenumber, bill.ToString(), dateconclude, podklcall,podklsms,podklinternet,tariff.idtariff.ToString(), provider.nameProv };
            return a;
        }
        /// <summary>
        /// метод изменения счета 
        /// </summary>
        /// <param name="cost">сумма начисления/списания</param>
        public void ChangeBill(double cost)
        {
            bill += cost;
        }
         
         ///<summary>
        /// метод для изменения пароля
        ///</summary>
        ///<param name="inputpasswd"> строка, на которую будет замене параметр passwd </param>
        public string ChangePassword(string oldpasswd,string inputpasswd)
        {
            if (oldpasswd.Equals(passwd))
            {
                this.passwd = inputpasswd;
                return ismeneno;
            }
            else return nesovpad;
        }
      
        public string Changesrevices(string pcall, string psms,string pinternet)
        {

            this.podklcall = pcall;
            this.podklsms=psms;
            this.podklinternet = pinternet;
            return ismeneno;
             
        }



    }
}
