﻿namespace Billing
{
    partial class DevelopAddClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbresult = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHome = new System.Windows.Forms.TextBox();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdRegister = new System.Windows.Forms.Button();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lbpasswd = new System.Windows.Forms.Label();
            this.lblOtchestvo = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.lblFamiliya = new System.Windows.Forms.Label();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.txtpasswd = new System.Windows.Forms.TextBox();
            this.txtOtchestvo = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtFamiliya = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numkv = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numkv)).BeginInit();
            this.SuspendLayout();
            // 
            // lbresult
            // 
            this.lbresult.AutoSize = true;
            this.lbresult.Location = new System.Drawing.Point(100, 254);
            this.lbresult.Name = "lbresult";
            this.lbresult.Size = new System.Drawing.Size(0, 13);
            this.lbresult.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 205);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Дом";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Улица";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Город";
            // 
            // txtHome
            // 
            this.txtHome.Location = new System.Drawing.Point(103, 205);
            this.txtHome.Name = "txtHome";
            this.txtHome.Size = new System.Drawing.Size(209, 20);
            this.txtHome.TabIndex = 25;
            // 
            // txtStreet
            // 
            this.txtStreet.Location = new System.Drawing.Point(103, 179);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(209, 20);
            this.txtStreet.TabIndex = 24;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(177, 279);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(135, 39);
            this.cmdCancel.TabIndex = 23;
            this.cmdCancel.Text = "Отмена";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdRegister
            // 
            this.cmdRegister.Location = new System.Drawing.Point(43, 279);
            this.cmdRegister.Name = "cmdRegister";
            this.cmdRegister.Size = new System.Drawing.Size(124, 39);
            this.cmdRegister.TabIndex = 22;
            this.cmdRegister.Text = "Зарегистрироваться";
            this.cmdRegister.UseVisualStyleBackColor = true;
            this.cmdRegister.Click += new System.EventHandler(this.cmdRegister_Click);
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(2, 138);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(38, 13);
            this.lblAddress.TabIndex = 21;
            this.lblAddress.Text = "Адрес";
            // 
            // lbpasswd
            // 
            this.lbpasswd.AutoSize = true;
            this.lbpasswd.Location = new System.Drawing.Point(40, 96);
            this.lbpasswd.Name = "lbpasswd";
            this.lbpasswd.Size = new System.Drawing.Size(45, 13);
            this.lbpasswd.TabIndex = 20;
            this.lbpasswd.Text = "Пароль";
            // 
            // lblOtchestvo
            // 
            this.lblOtchestvo.AutoSize = true;
            this.lblOtchestvo.Location = new System.Drawing.Point(43, 70);
            this.lblOtchestvo.Name = "lblOtchestvo";
            this.lblOtchestvo.Size = new System.Drawing.Size(54, 13);
            this.lblOtchestvo.TabIndex = 19;
            this.lblOtchestvo.Text = "Отчество";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(41, 44);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(29, 13);
            this.lbName.TabIndex = 18;
            this.lbName.Text = "Имя";
            // 
            // lblFamiliya
            // 
            this.lblFamiliya.AutoSize = true;
            this.lblFamiliya.Location = new System.Drawing.Point(43, 18);
            this.lblFamiliya.Name = "lblFamiliya";
            this.lblFamiliya.Size = new System.Drawing.Size(56, 13);
            this.lblFamiliya.TabIndex = 17;
            this.lblFamiliya.Text = "Фамилия";
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(103, 153);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(209, 20);
            this.txtTown.TabIndex = 16;
            // 
            // txtpasswd
            // 
            this.txtpasswd.Location = new System.Drawing.Point(103, 96);
            this.txtpasswd.Name = "txtpasswd";
            this.txtpasswd.Size = new System.Drawing.Size(209, 20);
            this.txtpasswd.TabIndex = 14;
            // 
            // txtOtchestvo
            // 
            this.txtOtchestvo.Location = new System.Drawing.Point(103, 70);
            this.txtOtchestvo.Name = "txtOtchestvo";
            this.txtOtchestvo.Size = new System.Drawing.Size(209, 20);
            this.txtOtchestvo.TabIndex = 13;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(103, 44);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(209, 20);
            this.txtName.TabIndex = 15;
            // 
            // txtFamiliya
            // 
            this.txtFamiliya.Location = new System.Drawing.Point(103, 18);
            this.txtFamiliya.Name = "txtFamiliya";
            this.txtFamiliya.Size = new System.Drawing.Size(209, 20);
            this.txtFamiliya.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 231);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Квартира";
            // 
            // numkv
            // 
            this.numkv.Location = new System.Drawing.Point(103, 231);
            this.numkv.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numkv.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numkv.Name = "numkv";
            this.numkv.Size = new System.Drawing.Size(209, 20);
            this.numkv.TabIndex = 30;
            this.numkv.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // DevelopAddClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 324);
            this.Controls.Add(this.numkv);
            this.Controls.Add(this.lbresult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtHome);
            this.Controls.Add(this.txtStreet);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdRegister);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.lbpasswd);
            this.Controls.Add(this.lblOtchestvo);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.lblFamiliya);
            this.Controls.Add(this.txtTown);
            this.Controls.Add(this.txtpasswd);
            this.Controls.Add(this.txtOtchestvo);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtFamiliya);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DevelopAddClient";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Добавление клиента";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DevelopAddClient_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numkv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbresult;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtHome;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdRegister;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lbpasswd;
        private System.Windows.Forms.Label lblOtchestvo;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lblFamiliya;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.TextBox txtpasswd;
        private System.Windows.Forms.TextBox txtOtchestvo;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtFamiliya;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numkv;
    }
}