﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class Developcontracts : Form
    {
        private string numb = "номер";
        private string bill = "Баланс";
        private string dateconclude = "дата закллючения контракта";
        private string podcall = "звонки";
        private string podsms = "sms";
        private string podinternet = "internet";
        private string tariff = "тариф";
        private string provider = "Провайдер";
        public DeveloperForm devf;
        public string numbcontractselected;
        public Developcontracts()
        {
            InitializeComponent();
        }



        private void Developcontracts_Load(object sender, EventArgs e)
        {
           listContract.Clear();

           cmdChange.Enabled = false;
           cmdDetail.Enabled = false;
           cmdPay.Enabled = false;
           cmdDelete.Enabled = false;
            DeveloperForm devform = this.Owner as DeveloperForm;

            listContract.Columns.Add(numb);
            listContract.Columns.Add(bill);
            listContract.Columns.Add(dateconclude);
            listContract.Columns.Add(podcall);
            listContract.Columns.Add(podsms);
            listContract.Columns.Add(podinternet);
            listContract.Columns.Add(tariff);
            listContract.Columns.Add(provider);
            int indexclient = devform.logformcopy.control.ExistClient(devform.numbclientselected);
            for (int i = 0; i < devform.logformcopy.control.clients[indexclient].contracts.Count; i++)
            {
                listContract.Items.Add(new ListViewItem(devform.logformcopy.control.clients[indexclient].contracts[i].getArr()));
            }
            listContract.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void Developcontracts_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void cmdBack_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Hide();
        }

        private void listContract_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            DeveloperForm devform = this.Owner as DeveloperForm;
            int indexclient = devform.logformcopy.control.ExistClient(devform.numbclientselected);
            
            List<string[]> lcontracts = new List<string[]>();
            foreach (var strtariff in devform.logformcopy.control.clients[indexclient].contracts)
                lcontracts.Add(strtariff.getArr());
            if (listContract.Tag.Equals("-1"))
                listContract.Tag = "1";
            else listContract.Tag = "-1";
            lcontracts.Sort(delegate(string[] a1, string[] a2)
            {
                if (e.Column == 1||e.Column == 6)
                {
                    int i1 = Convert.ToInt32(a1[e.Column]),
                        i2 = Convert.ToInt32(a2[e.Column]);
                    if (i1 > i2)
                        return (listContract.Tag.Equals("1") ? 1 : -1);
                    else if (i1 < i2)
                        return (listContract.Tag.Equals("1") ? -1 : 1);
                    else return 0;
                }
                else
                    if ((String.Compare(a1[e.Column], a2[e.Column]) == 1))
                        return (listContract.Tag.Equals("1") ? 1 : -1);
                    else if ((String.Compare(a1[e.Column], a2[e.Column]) == -1))
                        return (listContract.Tag.Equals("1") ? -1 : 1);
                    else return 0;

            });

            listContract.Items.Clear();
            foreach (var strmastariff in lcontracts)
                listContract.Items.Add(new ListViewItem(strmastariff));
            listContract.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void cmdPay_Click(object sender, EventArgs e)
        {
            DeveloperForm devform = this.Owner as DeveloperForm;
            devf = devform;
            DeveloperContractPayForm payf = new DeveloperContractPayForm();
            payf.Owner = this;
            payf.Show();
            this.Hide();
        }

        private void listContract_SelectedIndexChanged(object sender, EventArgs e)
        {

            cmdPay.Enabled = true;
            cmdDetail.Enabled = true;
            cmdChange.Enabled = true;
            cmdDelete.Enabled = true;
            if (!String.IsNullOrEmpty(listContract.FocusedItem.SubItems[0].Text))
                numbcontractselected = listContract.FocusedItem.SubItems[0].Text;
        }

        private void Developcontracts_Activated(object sender, EventArgs e)
        {
            listContract.Clear();
            DeveloperForm devform = this.Owner as DeveloperForm;
            if(String.IsNullOrEmpty(  numbcontractselected))
            {

                cmdChange.Enabled = false;
                cmdDetail.Enabled = false;
                cmdPay.Enabled = false;
                cmdDelete.Enabled = false;
            }
            listContract.Columns.Add(numb);
            listContract.Columns.Add(bill);
            listContract.Columns.Add(dateconclude);
            listContract.Columns.Add(podcall);
            listContract.Columns.Add(podsms);
            listContract.Columns.Add(podinternet);
            listContract.Columns.Add(tariff);
            listContract.Columns.Add(provider);
            int indexclient = devform.logformcopy.control.ExistClient(devform.numbclientselected);
            for (int i = 0; i < devform.logformcopy.control.clients[indexclient].contracts.Count; i++)
            {
                listContract.Items.Add(new ListViewItem(devform.logformcopy.control.clients[indexclient].contracts[i].getArr()));
            }
            listContract.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void cmdDetail_Click(object sender, EventArgs e)
        {
            DeveloperForm devform = this.Owner as DeveloperForm;
            devf = devform;
            Developercontractsdetailform detf = new Developercontractsdetailform();
            detf.Owner = this;
            detf.Show();
            this.Hide();
        }

        private void cmdDelete_Click(object sender, EventArgs e)
        {
            string message = "Вы хотите расторгнуть этот контракт. Вы уверены?";
            string caption = "Расторжение контракта";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(this, message, caption, buttons,
            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (result == DialogResult.Yes)
            {
                DeveloperForm devform = this.Owner as DeveloperForm;
                
                devform.logformcopy.control.DelContract(numbcontractselected);
                devform.logformcopy.control.Save();
                listContract.Clear();
                numbcontractselected = null;
                if (String.IsNullOrEmpty(numbcontractselected))
                {

                    cmdChange.Enabled = false;
                    cmdDetail.Enabled = false;
                    cmdPay.Enabled = false;
                    cmdDelete.Enabled = false;
                }
                listContract.Columns.Add(numb);
                listContract.Columns.Add(bill);
                listContract.Columns.Add(dateconclude);
                listContract.Columns.Add(podcall);
                listContract.Columns.Add(podsms);
                listContract.Columns.Add(podinternet);
                listContract.Columns.Add(tariff);
                listContract.Columns.Add(provider);
                int indexclient = devform.logformcopy.control.ExistClient(devform.numbclientselected);
                for (int i = 0; i < devform.logformcopy.control.clients[indexclient].contracts.Count; i++)
                {
                    listContract.Items.Add(new ListViewItem(devform.logformcopy.control.clients[indexclient].contracts[i].getArr()));
                }
                listContract.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                

            }
            
        }

        private void btnnewcontract_Click(object sender, EventArgs e)
        {
            DeveloperForm devform = this.Owner as DeveloperForm;
            devf = devform;
            DeveloperContractAddForm detf = new DeveloperContractAddForm();
            detf.Owner = this;
            detf.Show();
            this.Hide();

        }

        private void cmdChange_Click(object sender, EventArgs e)
        {
            DeveloperForm devform = this.Owner as DeveloperForm;
            devf = devform;
            DevelopContrectChangeForm dchange = new DevelopContrectChangeForm();
            dchange.Owner = this;
            dchange.Show();
            this.Hide();
        }
    }
}
