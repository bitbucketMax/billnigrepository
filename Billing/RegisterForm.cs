﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class RegisterForm : Form
    {
        private string dobavleno = "добавлено";
        private string vlogmen="вернуться на logform";
        private string vashlog="ваш логин: ";
        public string tariffselected;
        public RegisterForm()
        {
            
            InitializeComponent();
        }

        private void cmdRegister_Click(object sender, EventArgs e)
        {
            
            LogForm logform = this.Owner as LogForm;


            string result = logform.control.AddClient(txtFamiliya.Text, txtName.Text, txtOtchestvo.Text, "г. " + txtTown.Text + ", ул." + txtStreet.Text + ", д. " + txtHome.Text + ",кв. " + numkv.Value.ToString(), txtpasswd.Text);
            
            if (result.Equals(dobavleno))
            {
                lbresult.Text = vashlog + logform.control.clients[logform.control.clients.Count - 1].idclient.ToString();
                logform.control.Save();
                cmdCancel.Text = vlogmen;
            }
            else lbresult.Text = result;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Hide();
        }

        private void RegisterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }


      
      
    }
}
