﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
   public class Event
    {
        public Contract contract;
       public string date,service,volume,subnumber,cost;

       public Event( string date, string service, string volume, string number, string cost)
       {
           this.date = date;
           this.service = service;
           this.volume = volume;
           this.subnumber = number;
           this.cost = cost;
           
       }
       public string[] getArr()
       {
           string[] a = { date, service, volume, subnumber, cost };
           return a;
       }
    }
}
