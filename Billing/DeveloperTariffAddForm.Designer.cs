﻿namespace Billing
{
    partial class DeveloperTariffAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbcostmin = new System.Windows.Forms.Label();
            this.nummincost = new System.Windows.Forms.NumericUpDown();
            this.lbsmscost = new System.Windows.Forms.Label();
            this.numsmscost = new System.Windows.Forms.NumericUpDown();
            this.lbcostmb = new System.Windows.Forms.Label();
            this.nummbcost = new System.Windows.Forms.NumericUpDown();
            this.llbprov = new System.Windows.Forms.Label();
            this.listprovider = new System.Windows.Forms.ListView();
            this.btnAddtariff = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.lbCost = new System.Windows.Forms.Label();
            this.lbres = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nummincost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numsmscost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nummbcost)).BeginInit();
            this.SuspendLayout();
            // 
            // lbcostmin
            // 
            this.lbcostmin.AutoSize = true;
            this.lbcostmin.Location = new System.Drawing.Point(46, 36);
            this.lbcostmin.Name = "lbcostmin";
            this.lbcostmin.Size = new System.Drawing.Size(47, 13);
            this.lbcostmin.TabIndex = 0;
            this.lbcostmin.Text = "мин,руб";
            // 
            // nummincost
            // 
            this.nummincost.Location = new System.Drawing.Point(101, 36);
            this.nummincost.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nummincost.Name = "nummincost";
            this.nummincost.Size = new System.Drawing.Size(93, 20);
            this.nummincost.TabIndex = 1;
            this.nummincost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbsmscost
            // 
            this.lbsmscost.AutoSize = true;
            this.lbsmscost.Location = new System.Drawing.Point(46, 62);
            this.lbsmscost.Name = "lbsmscost";
            this.lbsmscost.Size = new System.Drawing.Size(47, 13);
            this.lbsmscost.TabIndex = 0;
            this.lbsmscost.Text = "смс,руб";
            // 
            // numsmscost
            // 
            this.numsmscost.Location = new System.Drawing.Point(101, 62);
            this.numsmscost.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numsmscost.Name = "numsmscost";
            this.numsmscost.Size = new System.Drawing.Size(93, 20);
            this.numsmscost.TabIndex = 1;
            this.numsmscost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbcostmb
            // 
            this.lbcostmb.AutoSize = true;
            this.lbcostmb.Location = new System.Drawing.Point(18, 88);
            this.lbcostmb.Name = "lbcostmb";
            this.lbcostmb.Size = new System.Drawing.Size(75, 13);
            this.lbcostmb.TabIndex = 0;
            this.lbcostmb.Text = "мегабайт,руб";
            // 
            // nummbcost
            // 
            this.nummbcost.Location = new System.Drawing.Point(101, 88);
            this.nummbcost.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nummbcost.Name = "nummbcost";
            this.nummbcost.Size = new System.Drawing.Size(93, 20);
            this.nummbcost.TabIndex = 1;
            this.nummbcost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // llbprov
            // 
            this.llbprov.AutoSize = true;
            this.llbprov.Location = new System.Drawing.Point(22, 118);
            this.llbprov.Name = "llbprov";
            this.llbprov.Size = new System.Drawing.Size(63, 13);
            this.llbprov.TabIndex = 0;
            this.llbprov.Text = "Провайдер";
            // 
            // listprovider
            // 
            this.listprovider.FullRowSelect = true;
            this.listprovider.Location = new System.Drawing.Point(101, 114);
            this.listprovider.Name = "listprovider";
            this.listprovider.Size = new System.Drawing.Size(86, 113);
            this.listprovider.TabIndex = 2;
            this.listprovider.UseCompatibleStateImageBehavior = false;
            this.listprovider.View = System.Windows.Forms.View.Details;
            this.listprovider.SelectedIndexChanged += new System.EventHandler(this.listprovider_SelectedIndexChanged);
            // 
            // btnAddtariff
            // 
            this.btnAddtariff.Location = new System.Drawing.Point(10, 258);
            this.btnAddtariff.Name = "btnAddtariff";
            this.btnAddtariff.Size = new System.Drawing.Size(75, 23);
            this.btnAddtariff.TabIndex = 3;
            this.btnAddtariff.Text = "Добавить";
            this.btnAddtariff.UseVisualStyleBackColor = true;
            this.btnAddtariff.Click += new System.EventHandler(this.btnAddtariff_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(146, 258);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Назад ";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lbCost
            // 
            this.lbCost.AutoSize = true;
            this.lbCost.Location = new System.Drawing.Point(31, 13);
            this.lbCost.Name = "lbCost";
            this.lbCost.Size = new System.Drawing.Size(62, 13);
            this.lbCost.TabIndex = 0;
            this.lbCost.Text = "Стоимость";
            // 
            // lbres
            // 
            this.lbres.AutoSize = true;
            this.lbres.Location = new System.Drawing.Point(31, 230);
            this.lbres.Name = "lbres";
            this.lbres.Size = new System.Drawing.Size(0, 13);
            this.lbres.TabIndex = 0;
            // 
            // DeveloperTariffAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(261, 289);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnAddtariff);
            this.Controls.Add(this.listprovider);
            this.Controls.Add(this.nummbcost);
            this.Controls.Add(this.lbres);
            this.Controls.Add(this.llbprov);
            this.Controls.Add(this.lbcostmb);
            this.Controls.Add(this.numsmscost);
            this.Controls.Add(this.lbsmscost);
            this.Controls.Add(this.nummincost);
            this.Controls.Add(this.lbCost);
            this.Controls.Add(this.lbcostmin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DeveloperTariffAddForm";
            this.Text = "Добавление тарифа";
            this.Activated += new System.EventHandler(this.DeveloperTariffAddForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeveloperTariffAddForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nummincost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numsmscost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nummbcost)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbcostmin;
        private System.Windows.Forms.NumericUpDown nummincost;
        private System.Windows.Forms.Label lbsmscost;
        private System.Windows.Forms.NumericUpDown numsmscost;
        private System.Windows.Forms.Label lbcostmb;
        private System.Windows.Forms.NumericUpDown nummbcost;
        private System.Windows.Forms.Label llbprov;
        private System.Windows.Forms.ListView listprovider;
        private System.Windows.Forms.Button btnAddtariff;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lbCost;
        private System.Windows.Forms.Label lbres;
    }
}