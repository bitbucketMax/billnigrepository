﻿namespace Billing
{
    partial class Detalisationform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.listdetaliz = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(176, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(189, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Вернуться в меню контракта";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listdetaliz
            // 
            this.listdetaliz.FullRowSelect = true;
            this.listdetaliz.Location = new System.Drawing.Point(3, 39);
            this.listdetaliz.Name = "listdetaliz";
            this.listdetaliz.Size = new System.Drawing.Size(362, 235);
            this.listdetaliz.TabIndex = 1;
            this.listdetaliz.Tag = "1";
            this.listdetaliz.UseCompatibleStateImageBehavior = false;
            this.listdetaliz.View = System.Windows.Forms.View.Details;
            this.listdetaliz.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listdetaliz_ColumnClick);
            // 
            // Detalisationform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 286);
            this.Controls.Add(this.listdetaliz);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Detalisationform";
            this.ShowIcon = false;
            this.Text = "Детализация";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Detalisationform_FormClosing);
            this.Load += new System.EventHandler(this.Detalisationform_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView listdetaliz;
    }
}