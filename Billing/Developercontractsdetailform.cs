﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class Developercontractsdetailform : Form
    {
        private string date = "дата";
        private string service = "сервис";
        private string volume = "объем";
        private string nomabon = "номер абонента";
        private string cost = "стоимость";
        private string serv;
        private string calls =  "call" ;
        private string smss =  "sms" ;
        private string internets =  "internet" ;
        private string[] call={"call"};
        private string[] sms={"sms"};
        private string[] internet={"internet"};
        private string listservname="Сервис";
        public Developercontractsdetailform()
        {
            InitializeComponent();
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Hide();
        }

        private void Developercontractsdetailform_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void Developercontractsdetailform_Load(object sender, EventArgs e)
        {
            Developcontracts devcontr = this.Owner as Developcontracts;
            btnaddevent.Enabled = false;
            numsubnum.Enabled = false;

            int indexcontract = devcontr.devf.logformcopy.control.ExistContract(devcontr.numbcontractselected);

            listdetaliz.Columns.Add(date);
            listdetaliz.Columns.Add(service);
            listdetaliz.Columns.Add(volume);
            listdetaliz.Columns.Add(nomabon);
            listdetaliz.Columns.Add(cost);
            int i = 0;
            for (i = 0; i <  devcontr.devf.logformcopy.control.contracts[indexcontract].events.Count; i++)
                listdetaliz.Items.Add(new ListViewItem(devcontr.devf.logformcopy.control.contracts[indexcontract].events[i].getArr()));
            
            listdetaliz.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            listserv.Columns.Add(listservname);
            listserv.Items.Add(new ListViewItem(call));
            listserv.Items.Add(new ListViewItem(sms));
            listserv.Items.Add(new ListViewItem(internet));

        }

        private void listserv_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnaddevent.Enabled = true;
            lbres.Text = "";
            
            if (!String.IsNullOrEmpty(listserv.FocusedItem.SubItems[0].Text))
                serv = listserv.FocusedItem.SubItems[0].Text;
            if(serv.Equals(calls))
            {
                numsubnum.Enabled = true;
                lbvolume.Text=volume+",мин";
            }
            if(serv.Equals(smss))
            {
                numsubnum.Enabled = true;
                lbvolume.Text =volume+ ",шт";
            }
            if (serv.Equals(internets))
            {
                numsubnum.Enabled = false;
                lbvolume.Text = volume+",мб";
            }
        }

        private void Developercontractsdetailform_Activated(object sender, EventArgs e)
        {
            Developcontracts devcontr = this.Owner as Developcontracts;
            if (String.IsNullOrEmpty(serv))
            {
                btnaddevent.Enabled = false;
                numsubnum.Enabled = false;
            }
            listdetaliz.Clear();
            listdetaliz.Columns.Add(date);
            listdetaliz.Columns.Add(service);
            listdetaliz.Columns.Add(volume);
            listdetaliz.Columns.Add(nomabon);
            listdetaliz.Columns.Add(cost);
            int indexcontract = devcontr.devf.logformcopy.control.ExistContract(devcontr.numbcontractselected);
            int i = 0;
            for (i = 0; i < devcontr.devf.logformcopy.control.contracts[indexcontract].events.Count; i++)
                listdetaliz.Items.Add(new ListViewItem(devcontr.devf.logformcopy.control.contracts[indexcontract].events[i].getArr()));
            listdetaliz.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

        }


        private void btnaddevent_Click(object sender, EventArgs e)
        {
            Developcontracts devcontr = this.Owner as Developcontracts;
            string subnum;
            if(!serv.Equals(internets))
            {
             subnum=numsubnum.Value.ToString();
            }else  
                 subnum="-";
            int indexcontr=devcontr.devf.logformcopy.control.ExistContract(devcontr.numbcontractselected);

            lbres.Text = devcontr.devf.logformcopy.control.AddEvent(System.DateTime.Now.ToString("dd:MM:yyyy")+" "+System.DateTime.Now.ToLongTimeString(), serv, numvolume.Value.ToString(), subnum, devcontr.devf.logformcopy.control.contracts[indexcontr].idcontract.ToString());
            devcontr.devf.logformcopy.control.Save();
            listdetaliz.Clear();
            listdetaliz.Columns.Add(date);
            listdetaliz.Columns.Add(service);
            listdetaliz.Columns.Add(volume);
            listdetaliz.Columns.Add(nomabon);
            listdetaliz.Columns.Add(cost);
            int indexcontract = devcontr.devf.logformcopy.control.ExistContract(devcontr.numbcontractselected);
            int i = 0;
            for (i = 0; i < devcontr.devf.logformcopy.control.contracts[indexcontract].events.Count; i++)
                listdetaliz.Items.Add(new ListViewItem(devcontr.devf.logformcopy.control.contracts[indexcontract].events[i].getArr()));
            listdetaliz.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

        }

        private void listdetaliz_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            Developcontracts devcontr = this.Owner as Developcontracts;
            if (e.Column != 3)
            {
                ContractMenu contractmenu = this.Owner as ContractMenu;
                int indexcontract = devcontr.devf.logformcopy.control.ExistContract(devcontr.numbcontractselected);
                List<string[]> levents = new List<string[]>();
                foreach (var strevent in devcontr.devf.logformcopy.control.contracts[indexcontract].events)
                    levents.Add(strevent.getArr());
                if (listdetaliz.Tag.Equals("-1"))
                    listdetaliz.Tag = "1";
                else listdetaliz.Tag = "-1";
                levents.Sort(delegate(string[] a1, string[] a2)
                {

                    if ((String.Compare(a1[e.Column], a2[e.Column]) == 1))
                        return (listdetaliz.Tag.Equals("1") ? 1 : -1);
                    else if ((String.Compare(a1[e.Column], a2[e.Column]) == -1))
                        return (listdetaliz.Tag.Equals("1") ? -1 : 1);
                    else return 0;

                });

                listdetaliz.Items.Clear();
                foreach (var strmasevents in levents)
                    listdetaliz.Items.Add(new ListViewItem(strmasevents));
                listdetaliz.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }
    }
}
