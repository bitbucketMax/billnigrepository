﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class LogForm : Form
    {
        public string idclient;
        private string passwd,result;
        public Controlclass control = new Controlclass(System.IO.Directory.GetCurrentDirectory());
        public LogForm()
        {
            control.Load();
            InitializeComponent();
        }
        private void cmdInit_Click(object sender, EventArgs e)
        {
            idclient=numidClient.Value.ToString();
            passwd = txtPassword.Text;
            result=control.Checkclient(idclient, passwd);
            checkclientres.Text=result;
            if (String.IsNullOrEmpty(result))
            { 
            MainMenu MainMenu = new MainMenu();
            
            MainMenu.Owner = this;
            MainMenu.Show();
           
            this.Hide();
            
            }
        }

        private void cmdRegister_Click(object sender, EventArgs e)
        {
            RegisterForm RegisterForm = new RegisterForm();
            RegisterForm.Show();
            RegisterForm.Owner = this;
            this.Hide();
        }

        private void cmdDesigner_Click(object sender, EventArgs e)
        {
            DeveloperForm DeveloperF = new DeveloperForm();
           
            DeveloperF.Owner = this; DeveloperF.Show();
            this.Hide();
        }
    }
}
