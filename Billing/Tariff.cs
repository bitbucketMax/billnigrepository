﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Billing
{
    public class Tariff
    {
        public int idtariff,smscost,mincost,mbcost;
        public string nameProv;
 
        public string[] getArr()
        {
            string[] a = { idtariff.ToString(), mincost.ToString(), smscost.ToString(), mbcost.ToString(), nameProv };
            return a;
        }

        public Tariff(int id,int mincost,int sms,int mbcost,string provider)
        {
            this.idtariff = id;
            this.smscost = sms;
            this.mincost = mincost;
            this.mbcost = mbcost;
            this.nameProv = provider;
        }

        
    }
}
