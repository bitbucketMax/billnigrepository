﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class DevelopAddClient : Form
    {
        private string dobavleno = "добавлено";
        private string vlogmen = "вернуться назад";
        private string vashlog = "новый логин: ";
        public string tariffselected;
        public DevelopAddClient()
        {
            InitializeComponent();
        }



        private void DevelopAddClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void cmdRegister_Click(object sender, EventArgs e)
        {
            DeveloperForm devform = this.Owner as DeveloperForm;

            string result = devform.logformcopy.control.AddClient(txtFamiliya.Text, txtName.Text, txtOtchestvo.Text, "г. " + txtTown.Text + ", ул." + txtStreet.Text + ", д. " + txtHome.Text+",кв. "+numkv.Value.ToString(), txtpasswd.Text);

            if (result.Equals(dobavleno))
            {
                lbresult.Text = vashlog + devform.logformcopy.control.clients[devform.logformcopy.control.clients.Count - 1].idclient.ToString();
                devform.logformcopy.control.Save();
                cmdCancel.Text = vlogmen;
            }
            else lbresult.Text = result;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Hide();
        }
    }
}
