﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class DeveloperProviderAdd : Form
    {
        public DeveloperProviderAdd()
        {
            InitializeComponent();
        }

        private void DeveloperProviderAdd_Load(object sender, EventArgs e)
        {
            btnAdd.Enabled = false;
        }

        private void txtnameprov_TextChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DeveloperForm devform = this.Owner as DeveloperForm;
          lbres.Text=  devform.logformcopy.control.AddProvider(txtnameprov.Text);

        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Hide();
        }

        private void DeveloperProviderAdd_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
