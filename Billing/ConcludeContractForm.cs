﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class ConcludeContractForm : Form
    {
        private string yes = "yes",no = "no",pcall,psms,pinternet,result;
        private string dobavleno="добавлено";
        private object vashid="ваш id: ";
        private object vashnumb="ваш номер: ";
        private string idtarif = "id Тарифа";
        private string pricmin = "1 минута, руб";
        private string prisms="1 смс, руб";
        private string primb=" 1 мб, руб";
        private string pname = "Провайдер";
        public string tariffselected;
      
        public ConcludeContractForm()
        {
            
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        { 

            if(podcall.Checked)
                pcall=yes;
            else pcall=no;
            if(podsms.Checked)
                psms=yes;
            else psms=no;
            if(podinternet.Checked)
                pinternet=yes;
            else pinternet=no;


            string dateconclude =DateTime.Now.ToString("dd MMMM yyyy");
            MainMenu MainMenuForm = this.Owner as MainMenu;
          result=  MainMenuForm.logformcopy.control.AddContract(txtpasswd.Text,tariffselected , dateconclude.ToString(), MainMenuForm.logformcopy.idclient, pcall, psms, pinternet);
          if (result.Equals(dobavleno))
          {
              lbres.Text = vashid + MainMenuForm.logformcopy.control.contracts[MainMenuForm.logformcopy.control.contracts.Count - 1].idcontract.ToString();
              lbnumb.Text = vashnumb + MainMenuForm.logformcopy.control.contracts[MainMenuForm.logformcopy.control.contracts.Count - 1].phonenumber;
              MainMenuForm.logformcopy.control.Save();
              ConcludeBtn.Enabled = false;
          }
          else lbres.Text = result;
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Hide();
            
        }

        private void ConcludeContractForm_Load(object sender, EventArgs e)
        {
            ConcludeBtn.Enabled = false;
            MainMenu MainMenuForm = this.Owner as MainMenu;
            listtariff.Columns.Add(idtarif);
            listtariff.Columns.Add(pricmin);
            listtariff.Columns.Add(prisms);
            listtariff.Columns.Add(primb);
            listtariff.Columns.Add(pname);
            
            for (int i = 0; i < MainMenuForm.logformcopy.control.tariffs.Count; i++)
            {
                listtariff.Items.Add(new ListViewItem(MainMenuForm.logformcopy.control.tariffs[i].getArr()));
            }
            listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

        }

        private void listtariff_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            MainMenu MainMenuForm = this.Owner as MainMenu;
            List<string[]> ltariff = new List<string[]>();
            foreach (var strtariff in MainMenuForm.logformcopy.control.tariffs)
                ltariff.Add(strtariff.getArr());
            if (listtariff.Tag.Equals("-1"))
                listtariff.Tag = "1";
            else listtariff.Tag = "-1";
            ltariff.Sort(delegate(string[] a1, string[] a2)
            {
                if (e.Column != 4)
                {
                    int i1 = Convert.ToInt32(a1[e.Column]),
                        i2 = Convert.ToInt32(a2[e.Column]);
                    if (i1 > i2)
                        return (listtariff.Tag.Equals("1") ? 1 : -1);
                    else if (i1 < i2)
                        return (listtariff.Tag.Equals("1") ? -1 : 1);
                    else return 0;
                }
                else
                    if ((String.Compare(a1[e.Column], a2[e.Column]) == 1))
                        return (listtariff.Tag.Equals("1") ? 1 : -1);
                    else if ((String.Compare(a1[e.Column], a2[e.Column]) == -1))
                        return (listtariff.Tag.Equals("1") ? -1 : 1);
                    else return 0;
                        
            });

            listtariff.Items.Clear();
            foreach (var strmastariff in ltariff)
                listtariff.Items.Add(new ListViewItem(strmastariff));
            listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void ConcludeContractForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void listtariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConcludeBtn.Enabled = true;
            if(!String.IsNullOrEmpty(listtariff.FocusedItem.SubItems[0].Text))
             tariffselected = listtariff.FocusedItem.SubItems[0].Text;
        }
    }
}
