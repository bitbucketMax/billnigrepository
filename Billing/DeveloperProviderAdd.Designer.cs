﻿namespace Billing
{
    partial class DeveloperProviderAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd = new System.Windows.Forms.Button();
            this.lbnameprov = new System.Windows.Forms.Label();
            this.txtnameprov = new System.Windows.Forms.TextBox();
            this.btncancel = new System.Windows.Forms.Button();
            this.lbres = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(12, 70);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lbnameprov
            // 
            this.lbnameprov.AutoSize = true;
            this.lbnameprov.Location = new System.Drawing.Point(40, 9);
            this.lbnameprov.Name = "lbnameprov";
            this.lbnameprov.Size = new System.Drawing.Size(135, 13);
            this.lbnameprov.TabIndex = 1;
            this.lbnameprov.Text = "Введите имя провайдера";
            // 
            // txtnameprov
            // 
            this.txtnameprov.Location = new System.Drawing.Point(53, 25);
            this.txtnameprov.Name = "txtnameprov";
            this.txtnameprov.Size = new System.Drawing.Size(100, 20);
            this.txtnameprov.TabIndex = 2;
            this.txtnameprov.TextChanged += new System.EventHandler(this.txtnameprov_TextChanged);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(123, 70);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(75, 23);
            this.btncancel.TabIndex = 0;
            this.btncancel.Text = "Назад";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // lbres
            // 
            this.lbres.AutoSize = true;
            this.lbres.Location = new System.Drawing.Point(40, 48);
            this.lbres.Name = "lbres";
            this.lbres.Size = new System.Drawing.Size(0, 13);
            this.lbres.TabIndex = 1;
            // 
            // DeveloperProviderAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(242, 109);
            this.Controls.Add(this.txtnameprov);
            this.Controls.Add(this.lbres);
            this.Controls.Add(this.lbnameprov);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnAdd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DeveloperProviderAdd";
            this.Text = "Новый провайдер";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeveloperProviderAdd_FormClosing);
            this.Load += new System.EventHandler(this.DeveloperProviderAdd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lbnameprov;
        private System.Windows.Forms.TextBox txtnameprov;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Label lbres;
    }
}