﻿namespace Billing
{
    partial class DevelopContrectChangeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numphone = new System.Windows.Forms.NumericUpDown();
            this.listtariff = new System.Windows.Forms.ListView();
            this.lbserviceschangeres = new System.Windows.Forms.Label();
            this.lbnumchangeres = new System.Windows.Forms.Label();
            this.lbpasswdchangeres = new System.Windows.Forms.Label();
            this.lbchangetariffres = new System.Windows.Forms.Label();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.btnpasswd = new System.Windows.Forms.Button();
            this.btnchangenumb = new System.Windows.Forms.Button();
            this.btnservice = new System.Windows.Forms.Button();
            this.ChtariffBtn = new System.Windows.Forms.Button();
            this.txtoldpasswd = new System.Windows.Forms.TextBox();
            this.txtpasswd = new System.Windows.Forms.TextBox();
            this.podinternet = new System.Windows.Forms.CheckBox();
            this.podsms = new System.Windows.Forms.CheckBox();
            this.podcall = new System.Windows.Forms.CheckBox();
            this.lbtariff = new System.Windows.Forms.Label();
            this.lbservenabled = new System.Windows.Forms.Label();
            this.lboldpasswd = new System.Windows.Forms.Label();
            this.lbnewnum = new System.Windows.Forms.Label();
            this.lbpasswd = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numphone)).BeginInit();
            this.SuspendLayout();
            // 
            // numphone
            // 
            this.numphone.Location = new System.Drawing.Point(227, 362);
            this.numphone.Maximum = new decimal(new int[] {
            -204313216,
            20,
            0,
            0});
            this.numphone.Minimum = new decimal(new int[] {
            -1194313216,
            20,
            0,
            0});
            this.numphone.Name = "numphone";
            this.numphone.Size = new System.Drawing.Size(183, 20);
            this.numphone.TabIndex = 44;
            this.numphone.Value = new decimal(new int[] {
            -1194313216,
            20,
            0,
            0});
            // 
            // listtariff
            // 
            this.listtariff.FullRowSelect = true;
            this.listtariff.Location = new System.Drawing.Point(12, 32);
            this.listtariff.Name = "listtariff";
            this.listtariff.Size = new System.Drawing.Size(377, 186);
            this.listtariff.TabIndex = 43;
            this.listtariff.Tag = "1";
            this.listtariff.UseCompatibleStateImageBehavior = false;
            this.listtariff.View = System.Windows.Forms.View.Details;
            this.listtariff.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listtariff_ColumnClick);
            this.listtariff.SelectedIndexChanged += new System.EventHandler(this.listtariff_SelectedIndexChanged);
            // 
            // lbserviceschangeres
            // 
            this.lbserviceschangeres.AutoSize = true;
            this.lbserviceschangeres.Location = new System.Drawing.Point(15, 396);
            this.lbserviceschangeres.Name = "lbserviceschangeres";
            this.lbserviceschangeres.Size = new System.Drawing.Size(0, 13);
            this.lbserviceschangeres.TabIndex = 42;
            // 
            // lbnumchangeres
            // 
            this.lbnumchangeres.AutoSize = true;
            this.lbnumchangeres.Location = new System.Drawing.Point(419, 396);
            this.lbnumchangeres.Name = "lbnumchangeres";
            this.lbnumchangeres.Size = new System.Drawing.Size(0, 13);
            this.lbnumchangeres.TabIndex = 41;
            // 
            // lbpasswdchangeres
            // 
            this.lbpasswdchangeres.AutoSize = true;
            this.lbpasswdchangeres.Location = new System.Drawing.Point(443, 294);
            this.lbpasswdchangeres.Name = "lbpasswdchangeres";
            this.lbpasswdchangeres.Size = new System.Drawing.Size(0, 13);
            this.lbpasswdchangeres.TabIndex = 40;
            // 
            // lbchangetariffres
            // 
            this.lbchangetariffres.AutoSize = true;
            this.lbchangetariffres.Location = new System.Drawing.Point(410, 75);
            this.lbchangetariffres.Name = "lbchangetariffres";
            this.lbchangetariffres.Size = new System.Drawing.Size(0, 13);
            this.lbchangetariffres.TabIndex = 39;
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(222, 2);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(167, 24);
            this.CancelBtn.TabIndex = 34;
            this.CancelBtn.Text = "Вернуться в меню контракта";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // btnpasswd
            // 
            this.btnpasswd.Location = new System.Drawing.Point(422, 257);
            this.btnpasswd.Name = "btnpasswd";
            this.btnpasswd.Size = new System.Drawing.Size(135, 26);
            this.btnpasswd.TabIndex = 37;
            this.btnpasswd.Text = "Изменить пароль";
            this.btnpasswd.UseVisualStyleBackColor = true;
            this.btnpasswd.Click += new System.EventHandler(this.btnpasswd_Click);
            // 
            // btnchangenumb
            // 
            this.btnchangenumb.Location = new System.Drawing.Point(422, 357);
            this.btnchangenumb.Name = "btnchangenumb";
            this.btnchangenumb.Size = new System.Drawing.Size(135, 26);
            this.btnchangenumb.TabIndex = 36;
            this.btnchangenumb.Text = "Изменить номер";
            this.btnchangenumb.UseVisualStyleBackColor = true;
            this.btnchangenumb.Click += new System.EventHandler(this.btnchangenumb_Click);
            // 
            // btnservice
            // 
            this.btnservice.Location = new System.Drawing.Point(9, 356);
            this.btnservice.Name = "btnservice";
            this.btnservice.Size = new System.Drawing.Size(135, 26);
            this.btnservice.TabIndex = 38;
            this.btnservice.Text = "Изменить список услуг";
            this.btnservice.UseVisualStyleBackColor = true;
            this.btnservice.Click += new System.EventHandler(this.btnservice_Click);
            // 
            // ChtariffBtn
            // 
            this.ChtariffBtn.Enabled = false;
            this.ChtariffBtn.Location = new System.Drawing.Point(422, 32);
            this.ChtariffBtn.Name = "ChtariffBtn";
            this.ChtariffBtn.Size = new System.Drawing.Size(135, 26);
            this.ChtariffBtn.TabIndex = 35;
            this.ChtariffBtn.Text = "Изменить тариф";
            this.ChtariffBtn.UseVisualStyleBackColor = true;
            this.ChtariffBtn.Click += new System.EventHandler(this.ChtariffBtn_Click);
            // 
            // txtoldpasswd
            // 
            this.txtoldpasswd.Location = new System.Drawing.Point(310, 261);
            this.txtoldpasswd.Name = "txtoldpasswd";
            this.txtoldpasswd.Size = new System.Drawing.Size(100, 20);
            this.txtoldpasswd.TabIndex = 33;
            // 
            // txtpasswd
            // 
            this.txtpasswd.Location = new System.Drawing.Point(310, 287);
            this.txtpasswd.Name = "txtpasswd";
            this.txtpasswd.Size = new System.Drawing.Size(100, 20);
            this.txtpasswd.TabIndex = 32;
            // 
            // podinternet
            // 
            this.podinternet.AutoSize = true;
            this.podinternet.Location = new System.Drawing.Point(18, 333);
            this.podinternet.Name = "podinternet";
            this.podinternet.Size = new System.Drawing.Size(72, 17);
            this.podinternet.TabIndex = 29;
            this.podinternet.Text = "интернет";
            this.podinternet.UseVisualStyleBackColor = true;
            // 
            // podsms
            // 
            this.podsms.AutoSize = true;
            this.podsms.Location = new System.Drawing.Point(18, 310);
            this.podsms.Name = "podsms";
            this.podsms.Size = new System.Drawing.Size(46, 17);
            this.podsms.TabIndex = 30;
            this.podsms.Text = "смс";
            this.podsms.UseVisualStyleBackColor = true;
            // 
            // podcall
            // 
            this.podcall.AutoSize = true;
            this.podcall.Location = new System.Drawing.Point(18, 287);
            this.podcall.Name = "podcall";
            this.podcall.Size = new System.Drawing.Size(62, 17);
            this.podcall.TabIndex = 31;
            this.podcall.Text = "звонок";
            this.podcall.UseVisualStyleBackColor = true;
            // 
            // lbtariff
            // 
            this.lbtariff.AutoSize = true;
            this.lbtariff.Location = new System.Drawing.Point(9, 8);
            this.lbtariff.Name = "lbtariff";
            this.lbtariff.Size = new System.Drawing.Size(128, 13);
            this.lbtariff.TabIndex = 24;
            this.lbtariff.Text = "Выберите другой тариф";
            // 
            // lbservenabled
            // 
            this.lbservenabled.AutoSize = true;
            this.lbservenabled.Location = new System.Drawing.Point(15, 261);
            this.lbservenabled.Name = "lbservenabled";
            this.lbservenabled.Size = new System.Drawing.Size(122, 13);
            this.lbservenabled.TabIndex = 25;
            this.lbservenabled.Text = "Подключаемые услуги";
            // 
            // lboldpasswd
            // 
            this.lboldpasswd.AutoSize = true;
            this.lboldpasswd.Location = new System.Drawing.Point(227, 264);
            this.lboldpasswd.Name = "lboldpasswd";
            this.lboldpasswd.Size = new System.Drawing.Size(84, 13);
            this.lboldpasswd.TabIndex = 27;
            this.lboldpasswd.Text = "Старый пароль";
            // 
            // lbnewnum
            // 
            this.lbnewnum.AutoSize = true;
            this.lbnewnum.Location = new System.Drawing.Point(224, 333);
            this.lbnewnum.Name = "lbnewnum";
            this.lbnewnum.Size = new System.Drawing.Size(76, 13);
            this.lbnewnum.TabIndex = 28;
            this.lbnewnum.Text = "Новый номер";
            // 
            // lbpasswd
            // 
            this.lbpasswd.AutoSize = true;
            this.lbpasswd.Location = new System.Drawing.Point(227, 287);
            this.lbpasswd.Name = "lbpasswd";
            this.lbpasswd.Size = new System.Drawing.Size(80, 13);
            this.lbpasswd.TabIndex = 26;
            this.lbpasswd.Text = "Новый пароль";
            // 
            // DevelopContrectChangeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 420);
            this.Controls.Add(this.numphone);
            this.Controls.Add(this.listtariff);
            this.Controls.Add(this.lbserviceschangeres);
            this.Controls.Add(this.lbnumchangeres);
            this.Controls.Add(this.lbpasswdchangeres);
            this.Controls.Add(this.lbchangetariffres);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.btnpasswd);
            this.Controls.Add(this.btnchangenumb);
            this.Controls.Add(this.btnservice);
            this.Controls.Add(this.ChtariffBtn);
            this.Controls.Add(this.txtoldpasswd);
            this.Controls.Add(this.txtpasswd);
            this.Controls.Add(this.podinternet);
            this.Controls.Add(this.podsms);
            this.Controls.Add(this.podcall);
            this.Controls.Add(this.lbtariff);
            this.Controls.Add(this.lbservenabled);
            this.Controls.Add(this.lboldpasswd);
            this.Controls.Add(this.lbnewnum);
            this.Controls.Add(this.lbpasswd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DevelopContrectChangeForm";
            this.Text = "Изменение контракта";
            this.Activated += new System.EventHandler(this.DevelopContrectChangeForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DevelopContrectChangeForm_FormClosing);
            this.Load += new System.EventHandler(this.DevelopContrectChangeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numphone)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numphone;
        private System.Windows.Forms.ListView listtariff;
        private System.Windows.Forms.Label lbserviceschangeres;
        private System.Windows.Forms.Label lbnumchangeres;
        private System.Windows.Forms.Label lbpasswdchangeres;
        private System.Windows.Forms.Label lbchangetariffres;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button btnpasswd;
        private System.Windows.Forms.Button btnchangenumb;
        private System.Windows.Forms.Button btnservice;
        private System.Windows.Forms.Button ChtariffBtn;
        private System.Windows.Forms.TextBox txtoldpasswd;
        private System.Windows.Forms.TextBox txtpasswd;
        private System.Windows.Forms.CheckBox podinternet;
        private System.Windows.Forms.CheckBox podsms;
        private System.Windows.Forms.CheckBox podcall;
        private System.Windows.Forms.Label lbtariff;
        private System.Windows.Forms.Label lbservenabled;
        private System.Windows.Forms.Label lboldpasswd;
        private System.Windows.Forms.Label lbnewnum;
        private System.Windows.Forms.Label lbpasswd;
    }
}