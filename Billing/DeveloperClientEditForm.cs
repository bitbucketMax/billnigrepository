﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class DeveloperClientEditForm : Form
    {
        public DeveloperClientEditForm()
        {
            InitializeComponent();
        }

        private void DeveloperClientEditForm_Activated(object sender, EventArgs e)
        {
            
        }

        private void DeveloperClientEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Owner.Show();
        }

        private void cmdChange_Click(object sender, EventArgs e)
        {
            DeveloperForm devform = this.Owner as DeveloperForm;

            int indexclient = devform.logformcopy.control.ExistClient(devform.numbclientselected);
            lbresfamilya.Text = devform.logformcopy.control.clients[indexclient].ChangeSurname(txtfamilia.Text);
            lbresname.Text = devform.logformcopy.control.clients[indexclient].ChangeName(txtname.Text);
            lbresoch.Text = devform.logformcopy.control.clients[indexclient].ChangeOtchestvo(txtothestvo.Text);
            lbresadr.Text = devform.logformcopy.control.clients[indexclient].ChangeAdress("г. " + txtTown.Text + ", ул." + txtStreet.Text + ", д. " + txtHome.Text + ",кв. " + numkv.Value.ToString());
            lbrespasswd.Text = devform.logformcopy.control.clients[indexclient].ChangePassword(txtoldpasswd.Text, txtnewpaswd.Text);
            devform.logformcopy.control.Save();
        }




    }
}
