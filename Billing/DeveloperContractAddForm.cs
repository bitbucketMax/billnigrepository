﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class DeveloperContractAddForm : Form
    {
        private string yes = "yes", no = "no", pcall, psms, pinternet, result;
        private string dobavleno = "добавлено";
        private object vashid = "новый id: ";
        private object vashnumb = "новый номер: ";
        private string idtarif = "id Тарифа";
        private string pricmin = "1 минута, руб";
        private string prisms = "1 смс, руб";
        private string primb = " 1 мб, руб";
        private string pname = "Провайдер";
        public string tariffselected;
        public DeveloperContractAddForm()
        {
            InitializeComponent();
        }

        private void DeveloperContractAddForm_Load(object sender, EventArgs e)
        {
            ConcludeBtn.Enabled = false;
            Developcontracts devcontr = this.Owner as Developcontracts;
            listtariff.Columns.Add(idtarif);
            listtariff.Columns.Add(pricmin);
            listtariff.Columns.Add(prisms);
            listtariff.Columns.Add(primb);
            listtariff.Columns.Add(pname);

            for (int i = 0; i < devcontr.devf.logformcopy.control.tariffs.Count; i++)
            {
                listtariff.Items.Add(new ListViewItem(devcontr.devf.logformcopy.control.tariffs[i].getArr()));
            }
            listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void DeveloperContractAddForm_Activated(object sender, EventArgs e)
        {

        }

        private void listtariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConcludeBtn.Enabled = true;
            if (!String.IsNullOrEmpty(listtariff.FocusedItem.SubItems[0].Text))
                tariffselected = listtariff.FocusedItem.SubItems[0].Text;
        }

        private void ConcludeBtn_Click(object sender, EventArgs e)
        {
            Developcontracts devcontr = this.Owner as Developcontracts;
            if (podcall.Checked)
                pcall = yes;
            else pcall = no;
            if (podsms.Checked)
                psms = yes;
            else psms = no;
            if (podinternet.Checked)
                pinternet = yes;
            else pinternet = no;


            string dateconclude = DateTime.Now.ToString("dd MMMM yyyy");

            result = devcontr.devf.logformcopy.control.AddContract(txtpasswd.Text, tariffselected, dateconclude.ToString(), devcontr.devf.numbclientselected, pcall, psms, pinternet);
            if (result.Equals(dobavleno))
            {
                lbres.Text = vashid + devcontr.devf.logformcopy.control.contracts[devcontr.devf.logformcopy.control.contracts.Count - 1].idcontract.ToString();
                lbnumb.Text = vashnumb + devcontr.devf.logformcopy.control.contracts[devcontr.devf.logformcopy.control.contracts.Count - 1].phonenumber;
                devcontr.devf.logformcopy.control.Save();
                ConcludeBtn.Enabled = false;
            }
            else lbres.Text = result;
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Hide();
            
        }

        private void listtariff_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            Developcontracts devcontr = this.Owner as Developcontracts;
            List<string[]> ltariff = new List<string[]>();
        foreach (var strtariff in devcontr.devf.logformcopy.control.tariffs)

                ltariff.Add(strtariff.getArr());
            if (listtariff.Tag.Equals("-1"))
                listtariff.Tag = "1";
            else listtariff.Tag = "-1";
            ltariff.Sort(delegate(string[] a1, string[] a2)
            {
                if (e.Column != 4)
                {
                    int i1 = Convert.ToInt32(a1[e.Column]),
                        i2 = Convert.ToInt32(a2[e.Column]);
                    if (i1 > i2)
                        return (listtariff.Tag.Equals("1") ? 1 : -1);
                    else if (i1 < i2)
                        return (listtariff.Tag.Equals("1") ? -1 : 1);
                    else return 0;
                }
                else
                    if ((String.Compare(a1[e.Column], a2[e.Column]) == 1))
                        return (listtariff.Tag.Equals("1") ? 1 : -1);
                    else if ((String.Compare(a1[e.Column], a2[e.Column]) == -1))
                        return (listtariff.Tag.Equals("1") ? -1 : 1);
                    else return 0;
                        
            });

            listtariff.Items.Clear();
            foreach (var strmastariff in ltariff)
                listtariff.Items.Add(new ListViewItem(strmastariff));
            listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void DeveloperContractAddForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
