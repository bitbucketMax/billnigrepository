﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
    public class Client
    {
 
       public int idclient;
       public string surname, name, otchestvo,adress ,password;
        
       public List<Contract> contracts = new List<Contract>();
       private string ismeneno="Изменено";
       private string invalidsimv="использование данных символов недопустимо";
       private string emtstr="";
       private string emptadr = "г. , ул., д. ,кв. 1";
       private string substrtownstreet = "г. , ул.";
       private string substrtownhome = ", д. ,кв. ";

       private string substrstreetnhome = ", ул., д. ";
       private const string zapvsepola = "заполните все поля адреса";
       private string nesovpad="Старый пароль не совпадает с текущим";
       private string invalidnewpasswd="Пустой пароль недопустим";

       public Client(int idclient,string surname, string name, string otchestvo,string adress,string passwd)
       {
           this.surname = surname;
           this.idclient = idclient;
           this.name = name;
           this.otchestvo = otchestvo;
           this.adress = adress;
           this.password = passwd;
       }
       public string[] getArr()
       {
           string[] a = { idclient.ToString(), surname, name, otchestvo, adress,contracts.Count().ToString() };
           return a;
       }
        
                

     ///<summary>
        /// метод для изменения имени
        ///</summary>
        ///<param name="inputname"> строка, на которую будет заменен параметр name </param>
        public string ChangeName(string inputname)
        {
            if (!String.IsNullOrEmpty(inputname))
            {
                string cheker = inputname;
                cheker = System.Text.RegularExpressions.Regex.Replace(cheker, @"[^a-zA-Zа-яА-Я]", string.Empty);
                if (cheker  .Equals( inputname))
                {
                    this.name = inputname;
                    return ismeneno;
                }
                else
                {
                    return invalidsimv;
                }
            }
            else return emtstr;
                     
            
                    }
        ///<summary>
        ///  метод для изменения фамилии
        ///</summary>
        ///<param name="inputsurname"> строка, на которую будет заменен параметр surname </param>
        public string ChangeSurname (string inputsurname)
        {
            if (!String.IsNullOrEmpty(inputsurname))
            {
                string cheker = inputsurname;
                cheker = System.Text.RegularExpressions.Regex.Replace(cheker, @"[^a-zA-Zа-яА-Я]", string.Empty);
                if (cheker.Equals(inputsurname))
                {
                    this.surname = inputsurname;
                    return ismeneno;
                }
                else
                {
                    return invalidsimv;
                }
            }
            else return emtstr;
                    }
        ///<summary>
        /// метод для изменения отчества
        ///</summary>
        ///<param name="inputotchestvo"> строка, на которую будет заменен параметр otchesto </param>
        public string ChangeOtchestvo (string inputotchestvo)
        {
            if (!String.IsNullOrEmpty(inputotchestvo))
            {
                string cheker = inputotchestvo;
                cheker = System.Text.RegularExpressions.Regex.Replace(cheker, @"[^a-zA-Zа-яА-Я]", string.Empty);
                if (cheker.Equals(inputotchestvo))
                {
                    this.otchestvo = inputotchestvo;
                    return ismeneno;
                }
                else
                {
                    return invalidsimv;
                }
            }
            else return emtstr;
        }
        ///<summary>
        /// метод для изменения адреса электронной почты
        ///</summary>
        ///<param name="inputadress"> строка, на которую будет заменен параметр adress </param>
        public string ChangeAdress (string adress)
        {
            if (!adress.Equals(emptadr))
            {

                if (!adress.Contains(substrtownstreet)  && !adress.Contains(substrstreetnhome) && !adress.Contains(substrtownhome))
                {

                    string cheker = adress;
                    cheker = System.Text.RegularExpressions.Regex.Replace(cheker, @"[^a-z, .A-Z0-9а-яА-Я/-]", string.Empty);
                    if (cheker.Equals(adress))
                    {
                        this.adress = adress;
                        return ismeneno;
                    }
                    else
                    {
                        return invalidsimv;
                    }
                } return zapvsepola;
            }
            else return emtstr;
        }
        ///<summary>
        /// метод для изменения пароля
        ///</summary>
        ///<param name="inputpasswd"> строка, на которую будет заменен параметр password </param>
        public string ChangePassword(string oldpasswd, string newpasswd)
        {
            if (!String.IsNullOrEmpty(oldpasswd))
            {
                if (!String.IsNullOrWhiteSpace(newpasswd))
                {
                    if (oldpasswd.Equals(this.password))
                    {
                        this.password = newpasswd;
                        return ismeneno;
                    }
                    else return nesovpad;
                }
                else return invalidnewpasswd;
            }
            else return emtstr;
        }
    }
}
