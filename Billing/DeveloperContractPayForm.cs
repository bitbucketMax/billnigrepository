﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class DeveloperContractPayForm : Form
    {
        public DeveloperContractPayForm()
        {
            InitializeComponent();
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            Developcontracts devcontr = this.Owner as Developcontracts;
            int indexcontract = devcontr.devf.logformcopy.control.ExistContract(devcontr.numbcontractselected);
            devcontr.devf.logformcopy.control.contracts[indexcontract].ChangeBill(Convert.ToDouble(summ.Value));
            devcontr.devf.logformcopy.control.Save();
            this.Owner.Show();
            this.Hide();
        }
    }
}
