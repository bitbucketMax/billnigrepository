﻿namespace Billing
{
    partial class LogForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.cmdDesigner = new System.Windows.Forms.Button();
            this.cmdInit = new System.Windows.Forms.Button();
            this.cmdRegister = new System.Windows.Forms.Button();
            this.numidClient = new System.Windows.Forms.NumericUpDown();
            this.checkclientres = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numidClient)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(75, 118);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(100, 20);
            this.txtPassword.TabIndex = 1;
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(31, 86);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(38, 13);
            this.lblLogin.TabIndex = 2;
            this.lblLogin.Text = "Логин";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(24, 121);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(45, 13);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Пароль";
            // 
            // cmdDesigner
            // 
            this.cmdDesigner.Location = new System.Drawing.Point(12, 8);
            this.cmdDesigner.Name = "cmdDesigner";
            this.cmdDesigner.Size = new System.Drawing.Size(118, 23);
            this.cmdDesigner.TabIndex = 4;
            this.cmdDesigner.Text = "Меню разработчика";
            this.cmdDesigner.UseVisualStyleBackColor = true;
            this.cmdDesigner.Click += new System.EventHandler(this.cmdDesigner_Click);
            // 
            // cmdInit
            // 
            this.cmdInit.Location = new System.Drawing.Point(75, 175);
            this.cmdInit.Name = "cmdInit";
            this.cmdInit.Size = new System.Drawing.Size(100, 23);
            this.cmdInit.TabIndex = 5;
            this.cmdInit.Text = "Войти";
            this.cmdInit.UseVisualStyleBackColor = true;
            this.cmdInit.Click += new System.EventHandler(this.cmdInit_Click);
            // 
            // cmdRegister
            // 
            this.cmdRegister.Location = new System.Drawing.Point(75, 204);
            this.cmdRegister.Name = "cmdRegister";
            this.cmdRegister.Size = new System.Drawing.Size(100, 23);
            this.cmdRegister.TabIndex = 6;
            this.cmdRegister.Text = "Регистрация";
            this.cmdRegister.UseVisualStyleBackColor = true;
            this.cmdRegister.Click += new System.EventHandler(this.cmdRegister_Click);
            // 
            // numidClient
            // 
            this.numidClient.Location = new System.Drawing.Point(75, 84);
            this.numidClient.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numidClient.Name = "numidClient";
            this.numidClient.Size = new System.Drawing.Size(100, 20);
            this.numidClient.TabIndex = 7;
            // 
            // checkclientres
            // 
            this.checkclientres.AutoSize = true;
            this.checkclientres.Location = new System.Drawing.Point(49, 146);
            this.checkclientres.Name = "checkclientres";
            this.checkclientres.Size = new System.Drawing.Size(0, 13);
            this.checkclientres.TabIndex = 8;
            // 
            // LogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(239, 238);
            this.Controls.Add(this.checkclientres);
            this.Controls.Add(this.numidClient);
            this.Controls.Add(this.cmdRegister);
            this.Controls.Add(this.cmdInit);
            this.Controls.Add(this.cmdDesigner);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblLogin);
            this.Controls.Add(this.txtPassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "LogForm";
            this.Text = "Авторизация";
            ((System.ComponentModel.ISupportInitialize)(this.numidClient)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Button cmdDesigner;
        private System.Windows.Forms.Button cmdInit;
        private System.Windows.Forms.Button cmdRegister;
        private System.Windows.Forms.NumericUpDown numidClient;
        private System.Windows.Forms.Label checkclientres;
    }
}

