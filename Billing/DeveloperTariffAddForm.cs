﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class DeveloperTariffAddForm : Form
    {
        private string pname = "Провайдер";
        public string numbProviderselected;
        private int width = 70;
        public DeveloperTariffAddForm()
        {
            InitializeComponent();
        }

        private void DeveloperTariffAddForm_Activated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(numbProviderselected))
            {
                btnAddtariff.Enabled = false;
            }
            DeveloperForm devform = this.Owner as DeveloperForm;
            listprovider.Clear();
            listprovider.Columns.Add(pname);

            listprovider.Columns[0].Width = width;
            for (int i = 0; i < devform.logformcopy.control.providers.Count; i++)
            {
                listprovider.Items.Add(new ListViewItem(devform.logformcopy.control.providers[i].getArr(pname)));
            }

        }

        private void DeveloperTariffAddForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }



        private void listprovider_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAddtariff.Enabled = true;
            if (!String.IsNullOrEmpty(listprovider.FocusedItem.SubItems[0].Text))
                numbProviderselected = listprovider.FocusedItem.SubItems[0].Text;
        }

        private void btnAddtariff_Click(object sender, EventArgs e)
        {
            DeveloperForm devform = this.Owner as DeveloperForm;
           lbres.Text= devform.logformcopy.control.AddTariff(nummincost.Value.ToString(), numsmscost.Value.ToString(), nummbcost.Value.ToString(), numbProviderselected);
           btnAddtariff.Enabled = false;
           numbProviderselected = null;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Owner.Show();
        }
    }
}
