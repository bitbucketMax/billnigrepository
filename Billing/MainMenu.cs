﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class MainMenu : Form
    {
        public LogForm logformcopy;
        private string numb="номер";
        private string bill="Баланс";
        private string dateconclude="дата закллючения контракта";
        private string podcall="звонки";
        private string podsms="sms";
        private string podinternet="internet";
        private string tariff="тариф";
        private string provider="Провайдер";
        private string nesovpadstr = "Пароль контракта и введенный пароль должны совпадать";
        private string emptstr = "";
        public string numbcontrselected;
        public MainMenu()
        {
            
            InitializeComponent();
        }

        private void cmdBack_Click(object sender, EventArgs e)
        {

            this.Hide();
            this.Owner.Show();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmdContractChoose.Enabled = true;
        }

        private void cmdContractChoose_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            logformcopy = logform;
            string passwd=txtContractPassword.Text;
            int indexcontr=logform.control.ExistContract(numbcontrselected);
            if (passwd.Equals(logform.control.contracts[indexcontr].passwd))
            {
            
            ContractMenu ContractMenu = new ContractMenu();
                 ContractMenu.Owner = this; 
                ContractMenu.Show();
                lbres.Text = emptstr;
            
            this.Hide();
            }else lbres.Text=nesovpadstr;
        }

        private void cmdChangePinfo_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            logformcopy = logform;
            ChangePInfoForm ChangePInfoForm = new ChangePInfoForm();
            ChangePInfoForm.Owner = this;
            this.Hide();
            ChangePInfoForm.Show();
        }

        private void cmdConclude_Click(object sender, EventArgs e)
        {

            LogForm logform = this.Owner as LogForm;
            logformcopy = logform;
            ConcludeContractForm ConcludeContractForm = new ConcludeContractForm();
            ConcludeContractForm.Owner = this;
            this.Hide();
            ConcludeContractForm.Show();
        }



        private void listContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmdContractChoose.Enabled = true;
            if (!String.IsNullOrEmpty(listContract.FocusedItem.SubItems[0].Text))
                numbcontrselected = listContract.FocusedItem.SubItems[0].Text;
        }

        private void MainMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void MainMenu_Activated(object sender, EventArgs e)
        {
            listContract.Clear();
            LogForm logform = this.Owner as LogForm;
            cmdContractChoose.Enabled = false;
            listContract.Columns.Add(numb);
            listContract.Columns.Add(bill);
            listContract.Columns.Add(dateconclude);
            listContract.Columns.Add(podcall);
            listContract.Columns.Add(podsms);
            listContract.Columns.Add(podinternet);
            listContract.Columns.Add(tariff);
            listContract.Columns.Add(provider);
            int indexclient = logform.control.ExistClient(logform.idclient);
            for (int i = 0; i < logform.control.clients[indexclient].contracts.Count; i++)
            {
                listContract.Items.Add(new ListViewItem(logform.control.clients[indexclient].contracts[i].getArr()));
            }
            listContract.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            
        }
    }
}
