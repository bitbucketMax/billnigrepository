﻿namespace Billing
{
    partial class DeveloperForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listClient = new System.Windows.Forms.ListView();
            this.listtariff = new System.Windows.Forms.ListView();
            this.listprovider = new System.Windows.Forms.ListView();
            this.lblistclient = new System.Windows.Forms.Label();
            this.lblistprov = new System.Windows.Forms.Label();
            this.lblisttariff = new System.Windows.Forms.Label();
            this.btnclientadd = new System.Windows.Forms.Button();
            this.btnclientdelete = new System.Windows.Forms.Button();
            this.btnclientchange = new System.Windows.Forms.Button();
            this.btnclientworkwithcontr = new System.Windows.Forms.Button();
            this.btntariffadd = new System.Windows.Forms.Button();
            this.btntariffdelete = new System.Windows.Forms.Button();
            this.btnprovideradd = new System.Windows.Forms.Button();
            this.btnproviderdelete = new System.Windows.Forms.Button();
            this.btnreturntologform = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listClient
            // 
            this.listClient.FullRowSelect = true;
            this.listClient.Location = new System.Drawing.Point(12, 31);
            this.listClient.Name = "listClient";
            this.listClient.Size = new System.Drawing.Size(649, 211);
            this.listClient.TabIndex = 0;
            this.listClient.Tag = "1";
            this.listClient.UseCompatibleStateImageBehavior = false;
            this.listClient.View = System.Windows.Forms.View.Details;
            this.listClient.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listClient_ColumnClick);
            this.listClient.SelectedIndexChanged += new System.EventHandler(this.listClient_SelectedIndexChanged);
            // 
            // listtariff
            // 
            this.listtariff.FullRowSelect = true;
            this.listtariff.Location = new System.Drawing.Point(12, 270);
            this.listtariff.Name = "listtariff";
            this.listtariff.Size = new System.Drawing.Size(391, 211);
            this.listtariff.TabIndex = 0;
            this.listtariff.Tag = "1";
            this.listtariff.UseCompatibleStateImageBehavior = false;
            this.listtariff.View = System.Windows.Forms.View.Details;
            this.listtariff.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listtariff_ColumnClick);
            this.listtariff.SelectedIndexChanged += new System.EventHandler(this.listtariff_SelectedIndexChanged);
            // 
            // listprovider
            // 
            this.listprovider.FullRowSelect = true;
            this.listprovider.Location = new System.Drawing.Point(12, 505);
            this.listprovider.Name = "listprovider";
            this.listprovider.Size = new System.Drawing.Size(391, 143);
            this.listprovider.TabIndex = 0;
            this.listprovider.Tag = "1";
            this.listprovider.UseCompatibleStateImageBehavior = false;
            this.listprovider.View = System.Windows.Forms.View.Details;
            this.listprovider.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listprovider_ColumnClick);
            this.listprovider.SelectedIndexChanged += new System.EventHandler(this.listprovider_SelectedIndexChanged);
            // 
            // lblistclient
            // 
            this.lblistclient.AutoSize = true;
            this.lblistclient.Location = new System.Drawing.Point(13, 12);
            this.lblistclient.Name = "lblistclient";
            this.lblistclient.Size = new System.Drawing.Size(94, 13);
            this.lblistclient.TabIndex = 1;
            this.lblistclient.Text = "Список клиентов";
            // 
            // lblistprov
            // 
            this.lblistprov.AutoSize = true;
            this.lblistprov.Location = new System.Drawing.Point(12, 489);
            this.lblistprov.Name = "lblistprov";
            this.lblistprov.Size = new System.Drawing.Size(113, 13);
            this.lblistprov.TabIndex = 1;
            this.lblistprov.Text = "Список провайдеров";
            // 
            // lblisttariff
            // 
            this.lblisttariff.AutoSize = true;
            this.lblisttariff.Location = new System.Drawing.Point(13, 254);
            this.lblisttariff.Name = "lblisttariff";
            this.lblisttariff.Size = new System.Drawing.Size(90, 13);
            this.lblisttariff.TabIndex = 1;
            this.lblisttariff.Text = "Список тарифов";
            // 
            // btnclientadd
            // 
            this.btnclientadd.Location = new System.Drawing.Point(664, 31);
            this.btnclientadd.Name = "btnclientadd";
            this.btnclientadd.Size = new System.Drawing.Size(100, 23);
            this.btnclientadd.TabIndex = 2;
            this.btnclientadd.Text = "Добавить";
            this.btnclientadd.UseVisualStyleBackColor = true;
            this.btnclientadd.Click += new System.EventHandler(this.btnclientadd_Click);
            // 
            // btnclientdelete
            // 
            this.btnclientdelete.Location = new System.Drawing.Point(664, 60);
            this.btnclientdelete.Name = "btnclientdelete";
            this.btnclientdelete.Size = new System.Drawing.Size(100, 23);
            this.btnclientdelete.TabIndex = 2;
            this.btnclientdelete.Text = "Удалить";
            this.btnclientdelete.UseVisualStyleBackColor = true;
            this.btnclientdelete.Click += new System.EventHandler(this.btnclientdelete_Click);
            // 
            // btnclientchange
            // 
            this.btnclientchange.Location = new System.Drawing.Point(664, 89);
            this.btnclientchange.Name = "btnclientchange";
            this.btnclientchange.Size = new System.Drawing.Size(100, 23);
            this.btnclientchange.TabIndex = 2;
            this.btnclientchange.Text = "Редактировать";
            this.btnclientchange.UseVisualStyleBackColor = true;
            this.btnclientchange.Click += new System.EventHandler(this.btnclientchange_Click);
            // 
            // btnclientworkwithcontr
            // 
            this.btnclientworkwithcontr.Location = new System.Drawing.Point(664, 118);
            this.btnclientworkwithcontr.Name = "btnclientworkwithcontr";
            this.btnclientworkwithcontr.Size = new System.Drawing.Size(100, 49);
            this.btnclientworkwithcontr.TabIndex = 2;
            this.btnclientworkwithcontr.Text = "Перейти к контрактам клиента";
            this.btnclientworkwithcontr.UseVisualStyleBackColor = true;
            this.btnclientworkwithcontr.Click += new System.EventHandler(this.btnclientworkwithcontr_Click);
            // 
            // btntariffadd
            // 
            this.btntariffadd.Location = new System.Drawing.Point(421, 270);
            this.btntariffadd.Name = "btntariffadd";
            this.btntariffadd.Size = new System.Drawing.Size(100, 23);
            this.btntariffadd.TabIndex = 2;
            this.btntariffadd.Text = "Добавить";
            this.btntariffadd.UseVisualStyleBackColor = true;
            this.btntariffadd.Click += new System.EventHandler(this.btntariffadd_Click);
            // 
            // btntariffdelete
            // 
            this.btntariffdelete.Location = new System.Drawing.Point(421, 299);
            this.btntariffdelete.Name = "btntariffdelete";
            this.btntariffdelete.Size = new System.Drawing.Size(100, 23);
            this.btntariffdelete.TabIndex = 2;
            this.btntariffdelete.Text = "Удалить";
            this.btntariffdelete.UseVisualStyleBackColor = true;
            this.btntariffdelete.Click += new System.EventHandler(this.btntariffdelete_Click);
            // 
            // btnprovideradd
            // 
            this.btnprovideradd.Location = new System.Drawing.Point(421, 505);
            this.btnprovideradd.Name = "btnprovideradd";
            this.btnprovideradd.Size = new System.Drawing.Size(94, 23);
            this.btnprovideradd.TabIndex = 2;
            this.btnprovideradd.Text = "Добавить";
            this.btnprovideradd.UseVisualStyleBackColor = true;
            this.btnprovideradd.Click += new System.EventHandler(this.btnprovideradd_Click);
            // 
            // btnproviderdelete
            // 
            this.btnproviderdelete.Location = new System.Drawing.Point(421, 534);
            this.btnproviderdelete.Name = "btnproviderdelete";
            this.btnproviderdelete.Size = new System.Drawing.Size(94, 23);
            this.btnproviderdelete.TabIndex = 2;
            this.btnproviderdelete.Text = "Удалить";
            this.btnproviderdelete.UseVisualStyleBackColor = true;
            this.btnproviderdelete.Click += new System.EventHandler(this.btnproviderdelete_Click);
            // 
            // btnreturntologform
            // 
            this.btnreturntologform.Location = new System.Drawing.Point(633, 607);
            this.btnreturntologform.Name = "btnreturntologform";
            this.btnreturntologform.Size = new System.Drawing.Size(100, 38);
            this.btnreturntologform.TabIndex = 3;
            this.btnreturntologform.Text = "Вернуться назад";
            this.btnreturntologform.UseVisualStyleBackColor = true;
            this.btnreturntologform.Click += new System.EventHandler(this.btnreturntologform_Click);
            // 
            // DeveloperForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 657);
            this.Controls.Add(this.btnreturntologform);
            this.Controls.Add(this.btnproviderdelete);
            this.Controls.Add(this.btnprovideradd);
            this.Controls.Add(this.btntariffdelete);
            this.Controls.Add(this.btntariffadd);
            this.Controls.Add(this.btnclientworkwithcontr);
            this.Controls.Add(this.btnclientchange);
            this.Controls.Add(this.btnclientdelete);
            this.Controls.Add(this.btnclientadd);
            this.Controls.Add(this.lblisttariff);
            this.Controls.Add(this.lblistprov);
            this.Controls.Add(this.lblistclient);
            this.Controls.Add(this.listprovider);
            this.Controls.Add(this.listtariff);
            this.Controls.Add(this.listClient);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DeveloperForm";
            this.Text = "Меню разработчика";
            this.Activated += new System.EventHandler(this.DeveloperForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeveloperForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listClient;
        private System.Windows.Forms.ListView listtariff;
        private System.Windows.Forms.ListView listprovider;
        private System.Windows.Forms.Label lblistclient;
        private System.Windows.Forms.Label lblistprov;
        private System.Windows.Forms.Label lblisttariff;
        private System.Windows.Forms.Button btnclientadd;
        private System.Windows.Forms.Button btnclientdelete;
        private System.Windows.Forms.Button btnclientchange;
        private System.Windows.Forms.Button btnclientworkwithcontr;
        private System.Windows.Forms.Button btntariffadd;
        private System.Windows.Forms.Button btntariffdelete;
        private System.Windows.Forms.Button btnprovideradd;
        private System.Windows.Forms.Button btnproviderdelete;
        private System.Windows.Forms.Button btnreturntologform;
    }
}