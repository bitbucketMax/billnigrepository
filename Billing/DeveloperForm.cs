﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class DeveloperForm : Form
    {
        private string idtarif = "id Тарифа";
        private string pricmin = "1 минута, руб";
        private string prisms = "1 смс, руб";
        private string primb = " 1 мб, руб";
        private string pname = "Провайдер";
        private string surname="Фамилия";
        private string name="Имя";
        private string idclient="id клиента";
        private string adress="Адрес";
        private string tariffcount="количество тарифов";
        private string nameprov="название провайдера";
        private string contractscount="количество контрактов";
        public string numbclientselected;
        public string numbtariffselected;
        public string numbProviderselected;
        public LogForm logformcopy;
    
        public DeveloperForm()
        {
            
            InitializeComponent();
           
            
        }

        private void DeveloperForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnreturntologform_Click(object sender, EventArgs e)
        {
            this.Owner.Show();
            this.Hide();
        }

        private void DeveloperForm_Activated(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(numbclientselected))
            {
                btnclientchange.Enabled = false;
                btnclientdelete.Enabled = false;
                btnclientworkwithcontr.Enabled = false;
            }
            if (String.IsNullOrEmpty(numbtariffselected))
            {

                btntariffdelete.Enabled = false;

            }
            if (String.IsNullOrEmpty(numbProviderselected))
            {

                btnproviderdelete.Enabled = false;

            }

            listClient.Clear();
            listtariff.Clear();
            listprovider.Clear();
            LogForm logform = this.Owner as LogForm;
            listtariff.Columns.Add(idtarif);
            listtariff.Columns.Add(pricmin);
            listtariff.Columns.Add(prisms);
            listtariff.Columns.Add(primb);
            listtariff.Columns.Add(pname);
            
            for (int i = 0; i < logform.control.tariffs.Count; i++)
            {
                listtariff.Items.Add(new ListViewItem(logform.control.tariffs[i].getArr()));
            }
            listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            listClient.Columns.Add(idclient);
            listClient.Columns.Add(surname);
            listClient.Columns.Add(name);
            listClient.Columns.Add(surname);
            listClient.Columns.Add(adress);
            listClient.Columns.Add(contractscount);
            for (int i = 0; i < logform.control.clients.Count; i++)
            {
                listClient.Items.Add(new ListViewItem(logform.control.clients[i].getArr()));
            }
            listClient.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            listprovider.Columns.Add(nameprov);
            listprovider.Columns.Add(tariffcount);
            listprovider.Columns.Add(contractscount);
            for(int i=0;i<logform.control.providers.Count;i++)
            {
                listprovider.Items.Add(new ListViewItem(logform.control.providers[i].getArr()));
            }
            listprovider.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void listtariff_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            List<string[]> ltariff = new List<string[]>();
            foreach (var strtariff in logform.control.tariffs)
                ltariff.Add(strtariff.getArr());
            if (listtariff.Tag.Equals("-1"))
                listtariff.Tag = "1";
            else listtariff.Tag = "-1";
            ltariff.Sort(delegate(string[] a1, string[] a2)
            {
                if (e.Column != 4)
                {
                    int i1 = Convert.ToInt32(a1[e.Column]),
                        i2 = Convert.ToInt32(a2[e.Column]);
                    if (i1 > i2)
                        return (listtariff.Tag.Equals("1") ? 1 : -1);
                    else if (i1 < i2)
                        return (listtariff.Tag.Equals("1") ? -1 : 1);
                    else return 0;
                }
                else
                    if ((String.Compare(a1[e.Column], a2[e.Column]) == 1))
                        return (listtariff.Tag.Equals("1") ? 1 : -1);
                    else if ((String.Compare(a1[e.Column], a2[e.Column]) == -1))
                        return (listtariff.Tag.Equals("1") ? -1 : 1);
                    else return 0;

            });

            listtariff.Items.Clear();
            foreach (var strmastariff in ltariff)
                listtariff.Items.Add(new ListViewItem(strmastariff));
            listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void listClient_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            List<string[]> lclient = new List<string[]>();
            foreach (var strtarclient in logform.control.clients)
                lclient.Add(strtarclient.getArr());
            if (listClient.Tag.Equals("-1"))
                listClient.Tag = "1";
            else listClient.Tag = "-1";
            lclient.Sort(delegate(string[] a1, string[] a2)
            {
                if (e.Column == 0 || e.Column == 5)
                {
                    int i1 = Convert.ToInt32(a1[e.Column]),
                        i2 = Convert.ToInt32(a2[e.Column]);
                    if (i1 > i2)
                        return (listClient.Tag.Equals("1") ? 1 : -1);
                    else if (i1 < i2)
                        return (listClient.Tag.Equals("1") ? -1 : 1);
                    else return 0;
                }
                else
                    if ((String.Compare(a1[e.Column], a2[e.Column]) == 1))
                        return (listClient.Tag.Equals("1") ? 1 : -1);
                    else if ((String.Compare(a1[e.Column], a2[e.Column]) == -1))
                        return (listClient.Tag.Equals("1") ? -1 : 1);
                    else return 0;

            });

            listClient.Items.Clear();
            foreach (var strmastariff in lclient)
                listClient.Items.Add(new ListViewItem(strmastariff));
            listClient.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void btnclientdelete_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            string message = "Вы хотите удалить этого клиента. Вы уверены?";
            string caption = "Удаление клиента";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(this, message, caption, buttons,
            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (result.Equals( DialogResult.Yes))
            {
                
                logform.control.DelClient(numbclientselected);
                logform.control.Save();
                numbclientselected = null;
                btnclientchange.Enabled = false;
                btnclientdelete.Enabled = false;
                btnclientworkwithcontr.Enabled = false;
                listClient.Clear();

                
                listClient.Columns.Add(idclient);
                listClient.Columns.Add(surname);
                listClient.Columns.Add(name);
                listClient.Columns.Add(surname);
                listClient.Columns.Add(adress);
                listClient.Columns.Add(contractscount);
                for (int i = 0; i < logform.control.clients.Count; i++)
                {
                    listClient.Items.Add(new ListViewItem(logform.control.clients[i].getArr()));
                }
                listClient.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
    

            }

            

        }

        private void listClient_SelectedIndexChanged(object sender, EventArgs e)
        {
           btnclientchange.Enabled = true;
           btnclientdelete.Enabled = true;
           btnclientworkwithcontr.Enabled = true;
            if (!String.IsNullOrEmpty(listClient.FocusedItem.SubItems[0].Text))
                numbclientselected = listClient.FocusedItem.SubItems[0].Text;
        }

        private void listtariff_SelectedIndexChanged(object sender, EventArgs e)
        {


            btntariffdelete.Enabled = true;
            if (!String.IsNullOrEmpty(listtariff.FocusedItem.SubItems[0].Text))
                numbtariffselected = listtariff.FocusedItem.SubItems[0].Text;
        }

        private void listprovider_SelectedIndexChanged(object sender, EventArgs e)
        {


            btnproviderdelete.Enabled = true;
            if (!String.IsNullOrEmpty(listprovider.FocusedItem.SubItems[0].Text))
                numbProviderselected = listprovider.FocusedItem.SubItems[0].Text;
        }

        private void listprovider_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            List<string[]> lprov = new List<string[]>();
            foreach (var strtarprov in logform.control.providers)
                lprov.Add(strtarprov.getArr());
            if (listprovider.Tag.Equals("-1"))
                listprovider.Tag = "1";
            else listprovider.Tag = "-1";
            lprov.Sort(delegate(string[] a1, string[] a2)
            {
                if (e.Column == 1 || e.Column == 2)
                {
                    int i1 = Convert.ToInt32(a1[e.Column]),
                        i2 = Convert.ToInt32(a2[e.Column]);
                    if (i1 > i2)
                        return (listprovider.Tag.Equals("1") ? 1 : -1);
                    else if (i1 < i2)
                        return (listprovider.Tag.Equals("1") ? -1 : 1);
                    else return 0;
                }
                else
                    if ((String.Compare(a1[e.Column], a2[e.Column]) == 1))
                        return (listprovider.Tag.Equals("1") ? 1 : -1);
                    else if ((String.Compare(a1[e.Column], a2[e.Column]) == -1))
                        return (listprovider.Tag.Equals("1") ? -1 : 1);
                    else return 0;
            });
            listprovider.Items.Clear();
            foreach (var strmastariff in lprov)
                listprovider.Items.Add(new ListViewItem(strmastariff));
            listprovider.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void btnclientadd_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            logformcopy = logform;
            DevelopAddClient devnewcl = new DevelopAddClient();
            devnewcl.Owner = this;
            devnewcl.Show();
            this.Hide();
        }

        private void btnclientworkwithcontr_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            logformcopy = logform;
            int indexclient = logform.control.ExistClient(numbclientselected);


            Developcontracts developcontrf = new Developcontracts();
            
            developcontrf.Owner = this;
            developcontrf.Show();
            this.Hide();
        }

        private void btnclientchange_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            logformcopy = logform;
            DeveloperClientEditForm devedform = new DeveloperClientEditForm();
            devedform.Owner = this;
            devedform.Show();
            this.Hide();
        }

        private void btntariffadd_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            logformcopy = logform;
            DeveloperTariffAddForm devaddtariffform = new DeveloperTariffAddForm();
            devaddtariffform.Owner = this;
            devaddtariffform.Show();
            this.Hide();
        }

        private void btntariffdelete_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            string message = "Вы хотите удалить тариф. Вы уверены?";
            string caption = "Удаление Тарифа";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(this, message, caption, buttons,
            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (result.Equals(DialogResult.Yes))
            {

                logform.control.DelTariff(numbtariffselected);
                logform.control.Save();
                numbclientselected = null;
                btnclientchange.Enabled = false;
                btnclientdelete.Enabled = false;
                btnclientworkwithcontr.Enabled = false;
                listtariff.Clear();
                listtariff.Columns.Add(idtarif);
                listtariff.Columns.Add(pricmin);
                listtariff.Columns.Add(prisms);
                listtariff.Columns.Add(primb);
                listtariff.Columns.Add(pname);

                for (int i = 0; i < logform.control.tariffs.Count; i++)
                {
                    listtariff.Items.Add(new ListViewItem(logform.control.tariffs[i].getArr()));
                }
                listprovider.Clear();
                listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                listprovider.Columns.Add(nameprov);
                listprovider.Columns.Add(tariffcount);
                listprovider.Columns.Add(contractscount);
                for (int i = 0; i < logform.control.providers.Count; i++)
                {
                    listprovider.Items.Add(new ListViewItem(logform.control.providers[i].getArr()));
                }
                listprovider.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                listClient.Clear();


                listClient.Columns.Add(idclient);
                listClient.Columns.Add(surname);
                listClient.Columns.Add(name);
                listClient.Columns.Add(surname);
                listClient.Columns.Add(adress);
                listClient.Columns.Add(contractscount);
                for (int i = 0; i < logform.control.clients.Count; i++)
                {
                    listClient.Items.Add(new ListViewItem(logform.control.clients[i].getArr()));
                }
                listClient.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

                btntariffdelete.Enabled = false;


            }
        }



        private void btnproviderdelete_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            string message = "Вы хотите удалить Провайдера. Вы уверены?";
            string caption = "Удаление Провайдера";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(this, message, caption, buttons,
            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (result.Equals(DialogResult.Yes))
            {

                logform.control.DelProvider(numbProviderselected);
                logform.control.Save();

                if (String.IsNullOrEmpty(numbclientselected))
                {
                    btnclientchange.Enabled = false;
                    btnclientdelete.Enabled = false;
                    btnclientworkwithcontr.Enabled = false;
                }
                if (String.IsNullOrEmpty(numbtariffselected))
                {

                    btntariffdelete.Enabled = false;

                }
                if (String.IsNullOrEmpty(numbProviderselected))
                {

                    btnproviderdelete.Enabled = false;

                }

                listClient.Clear();
                listtariff.Clear();
                listprovider.Clear();

                listtariff.Columns.Add(idtarif);
                listtariff.Columns.Add(pricmin);
                listtariff.Columns.Add(prisms);
                listtariff.Columns.Add(primb);
                listtariff.Columns.Add(pname);

                for (int i = 0; i < logform.control.tariffs.Count; i++)
                {
                    listtariff.Items.Add(new ListViewItem(logform.control.tariffs[i].getArr()));
                }
                listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                listClient.Columns.Add(idclient);
                listClient.Columns.Add(surname);
                listClient.Columns.Add(name);
                listClient.Columns.Add(surname);
                listClient.Columns.Add(adress);
                listClient.Columns.Add(contractscount);
                for (int i = 0; i < logform.control.clients.Count; i++)
                {
                    listClient.Items.Add(new ListViewItem(logform.control.clients[i].getArr()));
                }
                listClient.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                listprovider.Columns.Add(nameprov);
                listprovider.Columns.Add(tariffcount);
                listprovider.Columns.Add(contractscount);
                for (int i = 0; i < logform.control.providers.Count; i++)
                {
                    listprovider.Items.Add(new ListViewItem(logform.control.providers[i].getArr()));
                }
                listprovider.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                btnproviderdelete.Enabled = false;
            }

        }

        private void btnprovideradd_Click(object sender, EventArgs e)
        {
            LogForm logform = this.Owner as LogForm;
            logformcopy = logform;
            DeveloperProviderAdd devaddprovfform = new DeveloperProviderAdd();
            devaddprovfform.Owner = this;
            devaddprovfform.Show();
            this.Hide();
        }



    }
}
