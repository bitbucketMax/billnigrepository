﻿namespace Billing
{
    partial class Developcontracts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listContract = new System.Windows.Forms.ListView();
            this.cmdDelete = new System.Windows.Forms.Button();
            this.cmdChange = new System.Windows.Forms.Button();
            this.cmdPay = new System.Windows.Forms.Button();
            this.cmdDetail = new System.Windows.Forms.Button();
            this.cmdBack = new System.Windows.Forms.Button();
            this.btnnewcontract = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listContract
            // 
            this.listContract.FullRowSelect = true;
            this.listContract.Location = new System.Drawing.Point(0, 50);
            this.listContract.Name = "listContract";
            this.listContract.Size = new System.Drawing.Size(549, 204);
            this.listContract.TabIndex = 13;
            this.listContract.Tag = "1";
            this.listContract.UseCompatibleStateImageBehavior = false;
            this.listContract.View = System.Windows.Forms.View.Details;
            this.listContract.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listContract_ColumnClick);
            this.listContract.SelectedIndexChanged += new System.EventHandler(this.listContract_SelectedIndexChanged);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Location = new System.Drawing.Point(134, 319);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(279, 23);
            this.cmdDelete.TabIndex = 12;
            this.cmdDelete.Text = "Расторжение договора";
            this.cmdDelete.UseVisualStyleBackColor = true;
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdChange
            // 
            this.cmdChange.Location = new System.Drawing.Point(319, 260);
            this.cmdChange.Name = "cmdChange";
            this.cmdChange.Size = new System.Drawing.Size(117, 40);
            this.cmdChange.TabIndex = 11;
            this.cmdChange.Text = "Изменение данных договора";
            this.cmdChange.UseVisualStyleBackColor = true;
            this.cmdChange.Click += new System.EventHandler(this.cmdChange_Click);
            // 
            // cmdPay
            // 
            this.cmdPay.Location = new System.Drawing.Point(24, 260);
            this.cmdPay.Name = "cmdPay";
            this.cmdPay.Size = new System.Drawing.Size(104, 40);
            this.cmdPay.TabIndex = 10;
            this.cmdPay.Text = "Пополнение счета";
            this.cmdPay.UseVisualStyleBackColor = true;
            this.cmdPay.Click += new System.EventHandler(this.cmdPay_Click);
            // 
            // cmdDetail
            // 
            this.cmdDetail.Location = new System.Drawing.Point(134, 260);
            this.cmdDetail.Name = "cmdDetail";
            this.cmdDetail.Size = new System.Drawing.Size(99, 40);
            this.cmdDetail.TabIndex = 9;
            this.cmdDetail.Text = "Детализация";
            this.cmdDetail.UseVisualStyleBackColor = true;
            this.cmdDetail.Click += new System.EventHandler(this.cmdDetail_Click);
            // 
            // cmdBack
            // 
            this.cmdBack.Location = new System.Drawing.Point(388, 21);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Size = new System.Drawing.Size(153, 23);
            this.cmdBack.TabIndex = 8;
            this.cmdBack.Text = "Вернуться в меню разработчика";
            this.cmdBack.UseVisualStyleBackColor = true;
            this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
            // 
            // btnnewcontract
            // 
            this.btnnewcontract.Location = new System.Drawing.Point(442, 260);
            this.btnnewcontract.Name = "btnnewcontract";
            this.btnnewcontract.Size = new System.Drawing.Size(117, 40);
            this.btnnewcontract.TabIndex = 11;
            this.btnnewcontract.Text = "Новый договор";
            this.btnnewcontract.UseVisualStyleBackColor = true;
            this.btnnewcontract.Click += new System.EventHandler(this.btnnewcontract_Click);
            // 
            // Developcontracts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 346);
            this.Controls.Add(this.listContract);
            this.Controls.Add(this.cmdDelete);
            this.Controls.Add(this.btnnewcontract);
            this.Controls.Add(this.cmdChange);
            this.Controls.Add(this.cmdPay);
            this.Controls.Add(this.cmdDetail);
            this.Controls.Add(this.cmdBack);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Developcontracts";
            this.Text = "Контракты";
            this.Activated += new System.EventHandler(this.Developcontracts_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Developcontracts_FormClosing);
            this.Load += new System.EventHandler(this.Developcontracts_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listContract;
        private System.Windows.Forms.Button cmdDelete;
        private System.Windows.Forms.Button cmdChange;
        private System.Windows.Forms.Button cmdPay;
        private System.Windows.Forms.Button cmdDetail;
        private System.Windows.Forms.Button cmdBack;
        private System.Windows.Forms.Button btnnewcontract;
    }
}