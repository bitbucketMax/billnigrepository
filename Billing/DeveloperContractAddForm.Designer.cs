﻿namespace Billing
{
    partial class DeveloperContractAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listtariff = new System.Windows.Forms.ListView();
            this.lbnumb = new System.Windows.Forms.Label();
            this.lbres = new System.Windows.Forms.Label();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.ConcludeBtn = new System.Windows.Forms.Button();
            this.txtpasswd = new System.Windows.Forms.TextBox();
            this.podinternet = new System.Windows.Forms.CheckBox();
            this.podsms = new System.Windows.Forms.CheckBox();
            this.podcall = new System.Windows.Forms.CheckBox();
            this.lbtariff = new System.Windows.Forms.Label();
            this.lbservenabled = new System.Windows.Forms.Label();
            this.lbpasswd = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listtariff
            // 
            this.listtariff.FullRowSelect = true;
            this.listtariff.Location = new System.Drawing.Point(12, 35);
            this.listtariff.Name = "listtariff";
            this.listtariff.Size = new System.Drawing.Size(377, 186);
            this.listtariff.TabIndex = 21;
            this.listtariff.Tag = "1";
            this.listtariff.UseCompatibleStateImageBehavior = false;
            this.listtariff.View = System.Windows.Forms.View.Details;
            this.listtariff.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listtariff_ColumnClick);
            this.listtariff.SelectedIndexChanged += new System.EventHandler(this.listtariff_SelectedIndexChanged);
            // 
            // lbnumb
            // 
            this.lbnumb.AutoSize = true;
            this.lbnumb.Location = new System.Drawing.Point(465, 208);
            this.lbnumb.Name = "lbnumb";
            this.lbnumb.Size = new System.Drawing.Size(0, 13);
            this.lbnumb.TabIndex = 20;
            // 
            // lbres
            // 
            this.lbres.AutoSize = true;
            this.lbres.Location = new System.Drawing.Point(472, 180);
            this.lbres.Name = "lbres";
            this.lbres.Size = new System.Drawing.Size(0, 13);
            this.lbres.TabIndex = 19;
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(382, 247);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(128, 39);
            this.CancelBtn.TabIndex = 17;
            this.CancelBtn.Text = "Вернуться в меню клиента";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // ConcludeBtn
            // 
            this.ConcludeBtn.Location = new System.Drawing.Point(158, 247);
            this.ConcludeBtn.Name = "ConcludeBtn";
            this.ConcludeBtn.Size = new System.Drawing.Size(135, 39);
            this.ConcludeBtn.TabIndex = 18;
            this.ConcludeBtn.Text = "Заключить договор";
            this.ConcludeBtn.UseVisualStyleBackColor = true;
            this.ConcludeBtn.Click += new System.EventHandler(this.ConcludeBtn_Click);
            // 
            // txtpasswd
            // 
            this.txtpasswd.Location = new System.Drawing.Point(493, 138);
            this.txtpasswd.Name = "txtpasswd";
            this.txtpasswd.Size = new System.Drawing.Size(100, 20);
            this.txtpasswd.TabIndex = 16;
            // 
            // podinternet
            // 
            this.podinternet.AutoSize = true;
            this.podinternet.Location = new System.Drawing.Point(426, 85);
            this.podinternet.Name = "podinternet";
            this.podinternet.Size = new System.Drawing.Size(72, 17);
            this.podinternet.TabIndex = 13;
            this.podinternet.Text = "интернет";
            this.podinternet.UseVisualStyleBackColor = true;
            // 
            // podsms
            // 
            this.podsms.AutoSize = true;
            this.podsms.Location = new System.Drawing.Point(426, 62);
            this.podsms.Name = "podsms";
            this.podsms.Size = new System.Drawing.Size(46, 17);
            this.podsms.TabIndex = 14;
            this.podsms.Text = "смс";
            this.podsms.UseVisualStyleBackColor = true;
            // 
            // podcall
            // 
            this.podcall.AutoSize = true;
            this.podcall.Location = new System.Drawing.Point(426, 39);
            this.podcall.Name = "podcall";
            this.podcall.Size = new System.Drawing.Size(62, 17);
            this.podcall.TabIndex = 15;
            this.podcall.Text = "звонок";
            this.podcall.UseVisualStyleBackColor = true;
            // 
            // lbtariff
            // 
            this.lbtariff.AutoSize = true;
            this.lbtariff.Location = new System.Drawing.Point(9, 11);
            this.lbtariff.Name = "lbtariff";
            this.lbtariff.Size = new System.Drawing.Size(91, 13);
            this.lbtariff.TabIndex = 10;
            this.lbtariff.Text = "Выберите тариф";
            // 
            // lbservenabled
            // 
            this.lbservenabled.AutoSize = true;
            this.lbservenabled.Location = new System.Drawing.Point(423, 13);
            this.lbservenabled.Name = "lbservenabled";
            this.lbservenabled.Size = new System.Drawing.Size(122, 13);
            this.lbservenabled.TabIndex = 11;
            this.lbservenabled.Text = "Подключаемые услуги";
            // 
            // lbpasswd
            // 
            this.lbpasswd.AutoSize = true;
            this.lbpasswd.Location = new System.Drawing.Point(410, 138);
            this.lbpasswd.Name = "lbpasswd";
            this.lbpasswd.Size = new System.Drawing.Size(45, 13);
            this.lbpasswd.TabIndex = 12;
            this.lbpasswd.Text = "Пароль";
            // 
            // DeveloperContractAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 310);
            this.Controls.Add(this.listtariff);
            this.Controls.Add(this.lbnumb);
            this.Controls.Add(this.lbres);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.ConcludeBtn);
            this.Controls.Add(this.txtpasswd);
            this.Controls.Add(this.podinternet);
            this.Controls.Add(this.podsms);
            this.Controls.Add(this.podcall);
            this.Controls.Add(this.lbtariff);
            this.Controls.Add(this.lbservenabled);
            this.Controls.Add(this.lbpasswd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DeveloperContractAddForm";
            this.Text = "Добавление контракта";
            this.Activated += new System.EventHandler(this.DeveloperContractAddForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeveloperContractAddForm_FormClosing);
            this.Load += new System.EventHandler(this.DeveloperContractAddForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listtariff;
        private System.Windows.Forms.Label lbnumb;
        private System.Windows.Forms.Label lbres;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button ConcludeBtn;
        private System.Windows.Forms.TextBox txtpasswd;
        private System.Windows.Forms.CheckBox podinternet;
        private System.Windows.Forms.CheckBox podsms;
        private System.Windows.Forms.CheckBox podcall;
        private System.Windows.Forms.Label lbtariff;
        private System.Windows.Forms.Label lbservenabled;
        private System.Windows.Forms.Label lbpasswd;
    }
}