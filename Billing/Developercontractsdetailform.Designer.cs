﻿namespace Billing
{
    partial class Developercontractsdetailform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listdetaliz = new System.Windows.Forms.ListView();
            this.btnclose = new System.Windows.Forms.Button();
            this.lbaddevent = new System.Windows.Forms.Label();
            this.numsubnum = new System.Windows.Forms.NumericUpDown();
            this.numvolume = new System.Windows.Forms.NumericUpDown();
            this.lbservice = new System.Windows.Forms.Label();
            this.lbvolume = new System.Windows.Forms.Label();
            this.lbsubnum = new System.Windows.Forms.Label();
            this.btnaddevent = new System.Windows.Forms.Button();
            this.listserv = new System.Windows.Forms.ListView();
            this.lbres = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numsubnum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numvolume)).BeginInit();
            this.SuspendLayout();
            // 
            // listdetaliz
            // 
            this.listdetaliz.FullRowSelect = true;
            this.listdetaliz.Location = new System.Drawing.Point(12, 32);
            this.listdetaliz.Name = "listdetaliz";
            this.listdetaliz.Size = new System.Drawing.Size(411, 235);
            this.listdetaliz.TabIndex = 3;
            this.listdetaliz.Tag = "1";
            this.listdetaliz.UseCompatibleStateImageBehavior = false;
            this.listdetaliz.View = System.Windows.Forms.View.Details;
            this.listdetaliz.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listdetaliz_ColumnClick);
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(234, 3);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(189, 23);
            this.btnclose.TabIndex = 2;
            this.btnclose.Text = "Вернуться в меню контракта";
            this.btnclose.UseVisualStyleBackColor = true;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // lbaddevent
            // 
            this.lbaddevent.AutoSize = true;
            this.lbaddevent.Location = new System.Drawing.Point(12, 283);
            this.lbaddevent.Name = "lbaddevent";
            this.lbaddevent.Size = new System.Drawing.Size(116, 13);
            this.lbaddevent.TabIndex = 5;
            this.lbaddevent.Text = "Добавление cобытия";
            // 
            // numsubnum
            // 
            this.numsubnum.Location = new System.Drawing.Point(275, 329);
            this.numsubnum.Maximum = new decimal(new int[] {
            -194313217,
            20,
            0,
            0});
            this.numsubnum.Minimum = new decimal(new int[] {
            -1194313216,
            20,
            0,
            0});
            this.numsubnum.Name = "numsubnum";
            this.numsubnum.Size = new System.Drawing.Size(120, 20);
            this.numsubnum.TabIndex = 6;
            this.numsubnum.Value = new decimal(new int[] {
            -1194313216,
            20,
            0,
            0});
            // 
            // numvolume
            // 
            this.numvolume.Location = new System.Drawing.Point(137, 329);
            this.numvolume.Name = "numvolume";
            this.numvolume.Size = new System.Drawing.Size(120, 20);
            this.numvolume.TabIndex = 6;
            // 
            // lbservice
            // 
            this.lbservice.AutoSize = true;
            this.lbservice.Location = new System.Drawing.Point(12, 313);
            this.lbservice.Name = "lbservice";
            this.lbservice.Size = new System.Drawing.Size(114, 13);
            this.lbservice.TabIndex = 5;
            this.lbservice.Text = "Выберите вид услуги";
            // 
            // lbvolume
            // 
            this.lbvolume.AutoSize = true;
            this.lbvolume.Location = new System.Drawing.Point(134, 313);
            this.lbvolume.Name = "lbvolume";
            this.lbvolume.Size = new System.Drawing.Size(42, 13);
            this.lbvolume.TabIndex = 5;
            this.lbvolume.Text = "Объем";
            // 
            // lbsubnum
            // 
            this.lbsubnum.AutoSize = true;
            this.lbsubnum.Location = new System.Drawing.Point(283, 313);
            this.lbsubnum.Name = "lbsubnum";
            this.lbsubnum.Size = new System.Drawing.Size(89, 13);
            this.lbsubnum.TabIndex = 5;
            this.lbsubnum.Text = "номер абонента";
            // 
            // btnaddevent
            // 
            this.btnaddevent.Location = new System.Drawing.Point(198, 384);
            this.btnaddevent.Name = "btnaddevent";
            this.btnaddevent.Size = new System.Drawing.Size(189, 23);
            this.btnaddevent.TabIndex = 2;
            this.btnaddevent.Text = "Добавить событие";
            this.btnaddevent.UseVisualStyleBackColor = true;
            this.btnaddevent.Click += new System.EventHandler(this.btnaddevent_Click);
            // 
            // listserv
            // 
            this.listserv.Location = new System.Drawing.Point(15, 329);
            this.listserv.Name = "listserv";
            this.listserv.Size = new System.Drawing.Size(65, 81);
            this.listserv.TabIndex = 7;
            this.listserv.UseCompatibleStateImageBehavior = false;
            this.listserv.View = System.Windows.Forms.View.Details;
            this.listserv.SelectedIndexChanged += new System.EventHandler(this.listserv_SelectedIndexChanged);
            // 
            // lbres
            // 
            this.lbres.AutoSize = true;
            this.lbres.Location = new System.Drawing.Point(134, 368);
            this.lbres.Name = "lbres";
            this.lbres.Size = new System.Drawing.Size(0, 13);
            this.lbres.TabIndex = 5;
            // 
            // Developercontractsdetailform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 422);
            this.Controls.Add(this.listserv);
            this.Controls.Add(this.numvolume);
            this.Controls.Add(this.numsubnum);
            this.Controls.Add(this.lbsubnum);
            this.Controls.Add(this.lbvolume);
            this.Controls.Add(this.lbservice);
            this.Controls.Add(this.lbres);
            this.Controls.Add(this.lbaddevent);
            this.Controls.Add(this.listdetaliz);
            this.Controls.Add(this.btnaddevent);
            this.Controls.Add(this.btnclose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Developercontractsdetailform";
            this.Tag = "1";
            this.Text = "Детализация";
            this.Activated += new System.EventHandler(this.Developercontractsdetailform_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Developercontractsdetailform_FormClosing);
            this.Load += new System.EventHandler(this.Developercontractsdetailform_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numsubnum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numvolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listdetaliz;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.Label lbaddevent;
        private System.Windows.Forms.NumericUpDown numsubnum;
        private System.Windows.Forms.NumericUpDown numvolume;
        private System.Windows.Forms.Label lbservice;
        private System.Windows.Forms.Label lbvolume;
        private System.Windows.Forms.Label lbsubnum;
        private System.Windows.Forms.Button btnaddevent;
        private System.Windows.Forms.ListView listserv;
        private System.Windows.Forms.Label lbres;
    }
}