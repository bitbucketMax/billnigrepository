﻿namespace Billing
{
    partial class DeveloperClientEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numkv = new System.Windows.Forms.NumericUpDown();
            this.lbrespasswd = new System.Windows.Forms.Label();
            this.lbresadr = new System.Windows.Forms.Label();
            this.lbresoch = new System.Windows.Forms.Label();
            this.lbresname = new System.Windows.Forms.Label();
            this.lbresfamilya = new System.Windows.Forms.Label();
            this.lbresult = new System.Windows.Forms.Label();
            this.lbnewpasswd = new System.Windows.Forms.Label();
            this.lbnewpaswd = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbhome = new System.Windows.Forms.Label();
            this.lbstreet = new System.Windows.Forms.Label();
            this.ltown = new System.Windows.Forms.Label();
            this.txtnewpaswd = new System.Windows.Forms.TextBox();
            this.txtoldpasswd = new System.Windows.Forms.TextBox();
            this.txtHome = new System.Windows.Forms.TextBox();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdChange = new System.Windows.Forms.Button();
            this.lbadr = new System.Windows.Forms.Label();
            this.lbOtchestvo = new System.Windows.Forms.Label();
            this.lbname = new System.Windows.Forms.Label();
            this.lblFamiliya = new System.Windows.Forms.Label();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.txtothestvo = new System.Windows.Forms.TextBox();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtfamilia = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numkv)).BeginInit();
            this.SuspendLayout();
            // 
            // numkv
            // 
            this.numkv.Location = new System.Drawing.Point(103, 181);
            this.numkv.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numkv.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numkv.Name = "numkv";
            this.numkv.Size = new System.Drawing.Size(209, 20);
            this.numkv.TabIndex = 52;
            this.numkv.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbrespasswd
            // 
            this.lbrespasswd.AutoSize = true;
            this.lbrespasswd.Location = new System.Drawing.Point(337, 223);
            this.lbrespasswd.Name = "lbrespasswd";
            this.lbrespasswd.Size = new System.Drawing.Size(0, 13);
            this.lbrespasswd.TabIndex = 51;
            // 
            // lbresadr
            // 
            this.lbresadr.AutoSize = true;
            this.lbresadr.Location = new System.Drawing.Point(334, 136);
            this.lbresadr.Name = "lbresadr";
            this.lbresadr.Size = new System.Drawing.Size(0, 13);
            this.lbresadr.TabIndex = 50;
            // 
            // lbresoch
            // 
            this.lbresoch.AutoSize = true;
            this.lbresoch.Location = new System.Drawing.Point(334, 72);
            this.lbresoch.Name = "lbresoch";
            this.lbresoch.Size = new System.Drawing.Size(0, 13);
            this.lbresoch.TabIndex = 49;
            // 
            // lbresname
            // 
            this.lbresname.AutoSize = true;
            this.lbresname.Location = new System.Drawing.Point(334, 49);
            this.lbresname.Name = "lbresname";
            this.lbresname.Size = new System.Drawing.Size(0, 13);
            this.lbresname.TabIndex = 48;
            // 
            // lbresfamilya
            // 
            this.lbresfamilya.AutoSize = true;
            this.lbresfamilya.Location = new System.Drawing.Point(334, 20);
            this.lbresfamilya.Name = "lbresfamilya";
            this.lbresfamilya.Size = new System.Drawing.Size(0, 13);
            this.lbresfamilya.TabIndex = 47;
            // 
            // lbresult
            // 
            this.lbresult.AutoSize = true;
            this.lbresult.Location = new System.Drawing.Point(100, 275);
            this.lbresult.Name = "lbresult";
            this.lbresult.Size = new System.Drawing.Size(0, 13);
            this.lbresult.TabIndex = 46;
            // 
            // lbnewpasswd
            // 
            this.lbnewpasswd.AutoSize = true;
            this.lbnewpasswd.Location = new System.Drawing.Point(17, 246);
            this.lbnewpasswd.Name = "lbnewpasswd";
            this.lbnewpasswd.Size = new System.Drawing.Size(80, 13);
            this.lbnewpasswd.TabIndex = 45;
            this.lbnewpasswd.Text = "Новый пароль";
            // 
            // lbnewpaswd
            // 
            this.lbnewpaswd.AutoSize = true;
            this.lbnewpaswd.Location = new System.Drawing.Point(13, 217);
            this.lbnewpaswd.Name = "lbnewpaswd";
            this.lbnewpaswd.Size = new System.Drawing.Size(84, 13);
            this.lbnewpaswd.TabIndex = 44;
            this.lbnewpaswd.Text = "Старый пароль";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 183);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 43;
            this.label1.Text = "Квартира";
            // 
            // lbhome
            // 
            this.lbhome.AutoSize = true;
            this.lbhome.Location = new System.Drawing.Point(62, 155);
            this.lbhome.Name = "lbhome";
            this.lbhome.Size = new System.Drawing.Size(30, 13);
            this.lbhome.TabIndex = 42;
            this.lbhome.Text = "Дом";
            // 
            // lbstreet
            // 
            this.lbstreet.AutoSize = true;
            this.lbstreet.Location = new System.Drawing.Point(58, 129);
            this.lbstreet.Name = "lbstreet";
            this.lbstreet.Size = new System.Drawing.Size(39, 13);
            this.lbstreet.TabIndex = 41;
            this.lbstreet.Text = "Улица";
            // 
            // ltown
            // 
            this.ltown.AutoSize = true;
            this.ltown.Location = new System.Drawing.Point(58, 103);
            this.ltown.Name = "ltown";
            this.ltown.Size = new System.Drawing.Size(37, 13);
            this.ltown.TabIndex = 40;
            this.ltown.Text = "Город";
            // 
            // txtnewpaswd
            // 
            this.txtnewpaswd.Location = new System.Drawing.Point(103, 243);
            this.txtnewpaswd.Name = "txtnewpaswd";
            this.txtnewpaswd.Size = new System.Drawing.Size(209, 20);
            this.txtnewpaswd.TabIndex = 39;
            // 
            // txtoldpasswd
            // 
            this.txtoldpasswd.Location = new System.Drawing.Point(103, 217);
            this.txtoldpasswd.Name = "txtoldpasswd";
            this.txtoldpasswd.Size = new System.Drawing.Size(209, 20);
            this.txtoldpasswd.TabIndex = 38;
            // 
            // txtHome
            // 
            this.txtHome.Location = new System.Drawing.Point(103, 155);
            this.txtHome.Name = "txtHome";
            this.txtHome.Size = new System.Drawing.Size(209, 20);
            this.txtHome.TabIndex = 37;
            // 
            // txtStreet
            // 
            this.txtStreet.Location = new System.Drawing.Point(103, 129);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(209, 20);
            this.txtStreet.TabIndex = 36;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(317, 291);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(135, 39);
            this.cmdCancel.TabIndex = 35;
            this.cmdCancel.Text = "Возврат в меню ";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdChange
            // 
            this.cmdChange.Location = new System.Drawing.Point(82, 291);
            this.cmdChange.Name = "cmdChange";
            this.cmdChange.Size = new System.Drawing.Size(124, 39);
            this.cmdChange.TabIndex = 34;
            this.cmdChange.Text = "Изменить данные";
            this.cmdChange.UseVisualStyleBackColor = true;
            this.cmdChange.Click += new System.EventHandler(this.cmdChange_Click);
            // 
            // lbadr
            // 
            this.lbadr.AutoSize = true;
            this.lbadr.Location = new System.Drawing.Point(6, 103);
            this.lbadr.Name = "lbadr";
            this.lbadr.Size = new System.Drawing.Size(38, 13);
            this.lbadr.TabIndex = 33;
            this.lbadr.Text = "Адрес";
            // 
            // lbOtchestvo
            // 
            this.lbOtchestvo.AutoSize = true;
            this.lbOtchestvo.Location = new System.Drawing.Point(33, 65);
            this.lbOtchestvo.Name = "lbOtchestvo";
            this.lbOtchestvo.Size = new System.Drawing.Size(54, 13);
            this.lbOtchestvo.TabIndex = 32;
            this.lbOtchestvo.Text = "Отчество";
            // 
            // lbname
            // 
            this.lbname.AutoSize = true;
            this.lbname.Location = new System.Drawing.Point(58, 42);
            this.lbname.Name = "lbname";
            this.lbname.Size = new System.Drawing.Size(29, 13);
            this.lbname.TabIndex = 31;
            this.lbname.Text = "Имя";
            // 
            // lblFamiliya
            // 
            this.lblFamiliya.AutoSize = true;
            this.lblFamiliya.Location = new System.Drawing.Point(31, 20);
            this.lblFamiliya.Name = "lblFamiliya";
            this.lblFamiliya.Size = new System.Drawing.Size(56, 13);
            this.lblFamiliya.TabIndex = 30;
            this.lblFamiliya.Text = "Фамилия";
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(103, 103);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(209, 20);
            this.txtTown.TabIndex = 29;
            // 
            // txtothestvo
            // 
            this.txtothestvo.Location = new System.Drawing.Point(103, 65);
            this.txtothestvo.Name = "txtothestvo";
            this.txtothestvo.Size = new System.Drawing.Size(209, 20);
            this.txtothestvo.TabIndex = 27;
            // 
            // txtname
            // 
            this.txtname.Location = new System.Drawing.Point(103, 39);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(209, 20);
            this.txtname.TabIndex = 28;
            // 
            // txtfamilia
            // 
            this.txtfamilia.Location = new System.Drawing.Point(103, 13);
            this.txtfamilia.Name = "txtfamilia";
            this.txtfamilia.Size = new System.Drawing.Size(209, 20);
            this.txtfamilia.TabIndex = 26;
            // 
            // DeveloperClientEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 339);
            this.Controls.Add(this.numkv);
            this.Controls.Add(this.lbrespasswd);
            this.Controls.Add(this.lbresadr);
            this.Controls.Add(this.lbresoch);
            this.Controls.Add(this.lbresname);
            this.Controls.Add(this.lbresfamilya);
            this.Controls.Add(this.lbresult);
            this.Controls.Add(this.lbnewpasswd);
            this.Controls.Add(this.lbnewpaswd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbhome);
            this.Controls.Add(this.lbstreet);
            this.Controls.Add(this.ltown);
            this.Controls.Add(this.txtnewpaswd);
            this.Controls.Add(this.txtoldpasswd);
            this.Controls.Add(this.txtHome);
            this.Controls.Add(this.txtStreet);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdChange);
            this.Controls.Add(this.lbadr);
            this.Controls.Add(this.lbOtchestvo);
            this.Controls.Add(this.lbname);
            this.Controls.Add(this.lblFamiliya);
            this.Controls.Add(this.txtTown);
            this.Controls.Add(this.txtothestvo);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.txtfamilia);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "DeveloperClientEditForm";
            this.Text = "Изменение данных клиента";
            this.Activated += new System.EventHandler(this.DeveloperClientEditForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeveloperClientEditForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numkv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numkv;
        private System.Windows.Forms.Label lbrespasswd;
        private System.Windows.Forms.Label lbresadr;
        private System.Windows.Forms.Label lbresoch;
        private System.Windows.Forms.Label lbresname;
        private System.Windows.Forms.Label lbresfamilya;
        private System.Windows.Forms.Label lbresult;
        private System.Windows.Forms.Label lbnewpasswd;
        private System.Windows.Forms.Label lbnewpaswd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbhome;
        private System.Windows.Forms.Label lbstreet;
        private System.Windows.Forms.Label ltown;
        private System.Windows.Forms.TextBox txtnewpaswd;
        private System.Windows.Forms.TextBox txtoldpasswd;
        private System.Windows.Forms.TextBox txtHome;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdChange;
        private System.Windows.Forms.Label lbadr;
        private System.Windows.Forms.Label lbOtchestvo;
        private System.Windows.Forms.Label lbname;
        private System.Windows.Forms.Label lblFamiliya;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.TextBox txtothestvo;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.TextBox txtfamilia;
    }
}